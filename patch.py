from wgexport import *
from pprint import pprint

doTutorial = False # Change this to false for final 

class DummyElement(Element):
    def __init__(self,parent,node=None,**kwargs):
        super(DummyElement,self).__init__(parent,'Dummy',node=node)
        
moreHelp = '''
<html>
 <head>
  <title>{title}</title>
  <style>
  </style>
 </head>
 <body>
  <h1>{title}</h1>
  <h2>Rules</h2>
  <p>
    A PDF of the rules is available through the <b>Help</b> menu.
    Please note that it may take a little while for your PDF viewer
    to launch
  </p>
  <h2>Optional rules</h2>

  <p> At the start of a new game, you will be presented with a window
   to select which optional rules should be in effect.  You will
   <i>not</i> be able to change these settings later in the game.
   </p>

  <p> When the game is progressed to the first turn <b>Allied move
    points</b> phase, then game pieces that are not in used are
    removed and the optional rules are locked.  </p>

  <p> I you play by email, then one of you should select the optional
   rules, save the game <i> before</i> moving to the <b>Allied move
   points</b> phase. , and send it off to the other side for
   validation. Only then should you progress to the next phase.  </p>

  <h2>Historical setup</h2>

  <p><img class="icon" src="german-icon.png"/> &nbsp;Press the
    <b>Wehrmacht</b> button (<code>Ctrl-Shift-G</code>) to load the
    historical (more or less) German setup.  This is only available
    during the <b>Weather</b> phase of the first turn.</p>

  <p><img class="icon" src="allied-icon.png"/> &nbsp;Press the
   <b>SHAEF</b> button (<code>Ctrl-Shift-A</code>) to load the
   historical (more or less) Allied setup. This is only available in
   the <b>Allied invasion</b> phase of the first turn.</p>

  <p>These buttons are <i>only</i> available in the first turns
    <b>Weather</b> phase.</p>

  <h2>Turn and phase tracker</h2>

  <p> Use the <b>Turn tracker</b> interface in the menubar.  A number
    of automatic actions are implemented in this module, which heavily
    depend on the use of that interface.  </p>

  <p> You can go to the next phase by pressing the <b>+</b> button, or
    by pressing <code>Alt-T</code>.  This is the <i>only</i> possible
    way of moving the turn marker.</p>

  <p> Phases that are not in use - because some optional rule was
    disabled, like the <b>replacements</b> phases - are automatically
    skipped.  </p>

  <h2><img src="weather_flipped.png" width=24 height=24/>
  &nbsp;Weather</h2>

  <p> If the <b>Weather</b> optional rule is in effect, then the
    weather will automatically be resolved at the start of the
    <b>Weather</b> phase.  </p>

  <h2>Supply points</h2>

  <p> Next to each supply point on the map, are markers to keep track
    of which faction controls that supply point.  This <i>must</i> be
    managed by the factions - the module does not do this
    automatically.  </p>

  <p> A control marker can be &quot;flipped&quot; via the right-click
    context menu, or by selecting the marker and pressing
    <code>Ctrl-F</code> </p>

  <p> If the <b>Alternate move points</b> optional rule is in effect,
    then it is <i>crucial</i> that these are managed correctly, as
    they determine how many move points a faction receives </p>

  <h2>Move points</h2>

  <p>Move points are <i>automatically</i> allocated to the factions in
    the relevant phases, including if the <b>Alternate move points</b>
    optional rule is in effect.</p>

  <p>Move points are <i>not</i> automatically spent by movement and
    such.  The factions <i>must</i> take care to do so themselves.
    There are two buttons in the menubar of the OOBs to help with
    this.</p>

  <p>If the <b>Alternate move points</b> optional rule is in effect,
    it is <i>imperative</i> that the factions record control of supply
    points by flipping the faction markers on the map.</p>

  <h2>Reinforcements</h2>

  <p>Reinforcements units are <i>not</i> moved on to the map
    automatically.</p>

  <h2>Replacements</h2>

  <p>Only applicable if the optional <b>replacements</b> rule is in
    effect.</p>

  <p>Replacements <i>must</i> be built the turn before they can be
    taken as reinforcements.  Eliminated units are therefore placed
    face-down on the OOBs, and move points must be spend to bring them
    face up.  As with other movem point expendatures, this is
    <i>not</i> enforced by the module.</p>

  <p>Use the <b>Reinforce</b> (or <code>Ctrl-R</code>) context menu to
    bring a unit up to full strength.  This is not available unless
    the optional replacement rule is in effect.</p>

  <h2><img src="battle-marker-icon.png" width=24 height=24/> &nbsp;Battle
   declaration and resolution</h2>

  <p>Battles <i>must</i> be declared in a factions <b>movement</b>
    phase, including Allied strategic bombing missions.</p>

  <p>The module can automatically calculate odds and resolve combats
    via the use of <i>battle markers</i>.  To place a battle marker,
    select the attacking <i>and</i> defending units and press the
    battle marker button (<img src="battle-marker-icon.png"/>) or
    <code>Ctrl-X</code>.  This will place markers on the invovled
    units that identifiy the battle uniquely.</p>

  <p>This <i>only</i> works if the pieces are <i>on the map</i>.  That
    is, if the units are in an army holding box it will not work.</p>

  <p>If the user preference <b>Calculate Odds on battle
    declaration</b> is enabled (default), then the module will also
    calculate the odds (inluding bonuses and penalties for terrain),
    and place an odds marker (e.g., <img width=24 heigh=24
    src="odds_marker_0.png"/>) on one of the involved units.</p>

  <p>The rules says that only <i>one</i> hex may be attacked at a
    time. The module assumes this, but does not enforce it, so be
    careful to not select defending units in more than one hex.</p>

  <p>In the factions <b>Combat</b> phase, you can right click the odds
    marker and select <b>Resolve</b> (or <code>Ctrl-Y</code>) to
    resolve the combat.  The module will do the appropriate
    calculations and roll the dices needed, and a replace the odds
    marker with a <i>result marker</i> (e.g. <img width=24 height=24
    src="result_marker_DR.png"/>).</p>

  <p>The module will <i>not</i> apply the result to the invovled
    units. This <i>must</i> be done, according to the rules</i> by the
    factions.</p>

  <p>To clear a battle declaration, select one of the involved units
    or battle markers, and select <b>Clear</b> (or press
    <code>Ctrl-C</code>) from the context menu. All battles can be
    cleared by the <img src="clear-battles-icon.png"/> button in the
    menubar.</p>

  <h4>Step losses</h4>

  <p>Use the context menu item <b>Step Loss</b> (or
    <code>Ctrl-F</code>) to implement step losses. Units that are
    already reduced or has no step losses are automatically
    eliminated.</p>

  <h4><img src="br_bc_bw.png" width=24 height=24/> &nbsp;Allied strategic
  bombing missions</h4>

  <p>Allied strategic bombing missions, if that optional rules is
    enabled, can be declared in the same way as regular combat.  Note
    that the rules says that only <i>one</i> German unit per Allied
    bomber may be attacked.</p>

  <p>However, the mission should be resolved in the <b>Allied
    strategic air missions</b> phase, rather than in the <b>Combat</b>
    phase.  Resolution follows the same principle as above.</p>

  <p>If the result of a strategic bombing mission contains <b>NT</b>
    (no terrain bonuses), then an NT marker (<img
    src="result_marker_NT.png" width=24 height=24/>) is placed on the
    defending unit, <i>and</i> the terrain modifiers are automatically
    ignored.  Note that you <i>must</i> recalculate the odds by
    clearing the previously declared ground combat and redeclaring it.</p> 

  <p>If the optional rule <b>Alternate move points</b> is enabled,
    then the German faction is automatically awarded 1 MP per strategic
    bomber mission resolved by the Allied faction.</p>

  <h2>Eliminated units</h2>

  <p>Eliminated units will be sent back to the OOB.  Eliminated units
   are automatically flipped to their reduced side. This is to mark
   them as <i>eliminated</i> units and can therefore <i>not</i> be
   taken as reinforcements until the faction has spent the necessary
   move points (optional rule).</p>

  <h2><img src="isolated-icon.png"/> &nbsp;Supply</h2>

  <p>Use the right context menu item <b>Isolated</b> (or
    <code>Ctrl-I</code>) to mark one or more units as isolated.  The
    module <i>cannot</i> automatically detect out-of-supply
    conditions.</p>

  <p>If the optional rule <b>Step-loss on out of supply</b> is in
    force, then the module will <i>automatically</i> apply step losses
    (and possible eliminations) to <b>out-of-supply</b> units <i>at the
    end</i> of the supply phases. It is therefore important for the
    faction to ensure that the supply status of units is correct
    before the end of a supply phase.</p>

  <p>The menu button <b>Show unit status</b> (<code>Ctrl-S</code>)
    shows all units divided by supply status (flipped or with isolated
    marker). Use this to quickly find units that may need
    checking. Note that the module cannot by itself figure out the
    supply status of a unit.  This must be checked by a player.</p>

  <h2>About this game</h2>

  <p> This VASSAL module was created from L<sup>A</sup>T<sub>E</sub>X
   sources of a Print'n'Play version of the <b>D-Day</b> game.  That
   (a PDF) can be found at </p>

  <center>
    <code>https://gitlab.com/wargames_tex/sdday_tex</code></a>
  </center>

  <p> where this module, and the sources, can also be found.  The PDF
   can also be found as the rules of this game, available in the
   <b>Help</b> menu.  </p>

  <p> The original game was release by the Avalon Hill Game
   Company.</p>

  <h2>Changelog</h2>
  <dl>
    <dt>1.0</dt>
    <dd>Initial release</dd>
    <dt>2.0</dt>
      <ul>
        <li>Battle markers</li>
        <li>Automatic odds calculations</li>
        <li>Automatic battle resolution</li>
        <li>Automatic move points</li>
        <li>Optional rules interface</li>
      </ul>
    <dt>2.1</dt>
    <dd>Some fixes for automation</dd>
    <dt>2.2</dt>
    <dd>Fading drop shadows on counters</dd>
  </dl>
        
  <h2>Credits</h2>
  <dl>
   <dt>Design, development &amp; rules:</dt>
   <dd>Samuel Craig Taylor, Jr.</dd>
   <dt>Cover art:</dt><dd>George Parrish</dd>
   <dt>Graphics &amp; typesetting:</dt><dd>Charles Kibler</dd>
   <dt>Packaging:</dt><dd>Monarch Services &amp; Eastern Box</dd>
   <dt>Playtesters:</dt>
   <dd>Jeff Buchaneau, Kevin Hewitt, George Petronis, Edward Philips,
     Everett Post, James Turner, &amp; Byron Wolfe</dd>
  </dl>

  <h2>Copyright and license</h2>

  <p> This work is &#127279; 2022 Christian Holm Christensen, and
   licensed under the Creative Commons Attribution-ShareAlike 4.0
   International License. To view a copy of this license, visit </p>

  <center>
   <code>http://creativecommons.org/licenses/by-sa/4.0</code>
  </center>

  <p>
    or send a letter to
  </p>

  <center>
   Creative Commons<br>
   PO Box 1866<br>
   Mountain View<br>
   CA 94042<br>
   USA
  </center>
</body>
</html>'''

# --------------------------------------------------------------------
def concatHexes(hexes):
    return ':'+':'.join(hexes) + ':'
# --------------------------------------------------------------------
def getPortHexes():
    ports = [
        'E3',	# Hamburg
        'E4',	# Wilhemshaven
        'D7',	# Amsterdam
        'E9',	# Antwerp
        'D10',	# Dunkrik
        'D11',	# Calais
        'E14',	# Diepe
        'D14',	# Le Havre
        'C16',	# Cherbourg
        'B19',	# Brest
        'D19',	# Lorient
        'F18',	# Nantes
        'J19',	# Bordeaux
        'R16',	# Marseilles
        'A14',	# Southampton
        'C12',	# Dover
        'B12',	# London
        'T19',	# Mediterranean
    ]
    return concatHexes(ports)
# --------------------------------------------------------------------
def getHexes(hexes,what):
    return [k for k,v in hexes.items() if what in v]
# --------------------------------------------------------------------
def getWoodsHexes(hexes):
    return concatHexes(getHexes(hexes,'woods'))
# --------------------------------------------------------------------
def getSwampHexes(hexes):
    return concatHexes(getHexes(hexes,'swamp'))
# --------------------------------------------------------------------
def getMountainsHexes(hexes):
    return concatHexes(getHexes(hexes,'mountains'))
# --------------------------------------------------------------------
def getFortifiedHexes(hexes):
    return concatHexes(getHexes(hexes,'fortified'))
# --------------------------------------------------------------------
def getCityHexes(hexes):
    return concatHexes(getHexes(hexes,'city'))
# --------------------------------------------------------------------
def getFortHexes(hexes):
    return concatHexes(getHexes(hexes,'fort'))
# --------------------------------------------------------------------
def getBritainHexes(hexes):
    return concatHexes(getHexes(hexes,'britain'))
# --------------------------------------------------------------------
def getContinentHexes(hexes):
    return concatHexes([k for k,v in hexes.items() if 'britain' not in v])
# --------------------------------------------------------------------
def getInvasionCrossings():
    # M10 -> L9      L9  -> M9    L9.NE -M10.NW  c1=even  dr=+1
    # M15 -> L14     L14 -> M14   L14.NE-M15.NW  c1=even  dr-+1
    # N16 -> M16     M16 -> N15   M15.SE-N16.NW  c1=odd   dr=+1
    # N20 -> M20     M20 -> N19   M20.NE-N20.NW  c1=odd   dr= 0
    # K17 -> J16     J16 -> K16   J16.NE-K17.NW  c1=even  dr=+1
    # I18 -> H17     H17 -> I17   H17.NE-I18.NW  c1=even  dr=+1
    # (J9->I9)       I9  -> J8    I8.SE -J9.NW   c1=odd   dr=+1
    # (L8->K8)       K8  -> L7    K8.NE -L8.NW   c1=odd   dr= 0
    # K8  -> J7      J7  -> K7    J7.NE -K8.NW   c1=even  dr=+1
    # H12 -> G13     G13 -> H13   G13.SE-H12.SW  c1=odd   dr=-1
    # H8  -> G8      G8  -> H7    G8.NE -H8.NW   c1=odd   dr= 0
    # G8  -> F7      F7  -> G7    F7.NE -G8.NW   c1=even  dr=+1
    # E16 -> D15     D15 -> E15   D15.NE-E16.NW  c1=even  dr=+1
    invasions = {
        'C8':   ['D7'],			# 2
        'C9':	['D8'],			# 2
        'D9':	['E10'],		# 2
        'C10':	['D10'],		# 1
        'C11':	['D11'],		# 1
        'D13':	['D12','E13','E14'],	# 1
        'C14':	['D14'],		# 1
        'C15':	['E15','D15'],		# 1
        'B15':	['C16'],		# 1
        'B17':	['C18'],		# 1
        'A18':	['B18'],		# 1
        'A19':	['B19'],		# 2
        'D20':	['D19'],		# 2
        'E20':	['E19'],		# 2
        'F19':	['F18'],		# 2
        'H20':	['I20'],		# 3
        'R18':	['Q19'],		# 2
        'Q18':	['P18','P17'],		# 2
        'R17':	['Q17'],		# 1
        'S17':	['R16'],		# 1
        'T16':	['S16'],		# 1
    }
    return ':'+':'.join([':'.join([f'{f}{s}'for s in sec])
                         for f,sec in invasions.items()]) + ':'
# --------------------------------------------------------------------
def getRiverCrossings():
    rivers = [
        # Elbe
        ['E3.SW','E3.SE','E3.E','F2.SE','F2.E','G2.SE','G2.E','H1.SE',
         'I2.SW','I2.SE','J2.NW','J2.NE','K2.SW','K2.SE','L2.NW',
         'L2.NE','M2.NW','M2.NE','N1.SW','N2.W','N2.SW','N3.W'
         ],
        # Around Berlin
        [ 'H2.NE', 'I2.NW', 'I1.W'],
        [ 'I2.NW', 'I1.SE', 'I1.E'],
        # Vltava
        [ 'N1.SW','N1.SE','O2.SW','O2.SE'],
        # Maine
        [ 'I3.NE','I3.E','J3.NE','J3.E','J3.SE','J4.E','K5.NE', 'K5.E',
          'K5.SE','K6.E','K6.SE','K6.SW','K7.W'],
        # South of Leipzig
        [ 'K3.SW','K3.SE'],
        # Weser
        [ 'F3.W','F3.SW','F3.SE','G5.NW','G5.NE','G5.E' ,'H5.NE','H5.E'],
        # South of Hamburg
        ['F3.SE','F3.E','G3.SE','G3.E'],
        # Ems
        [ 'D5.NE','D5.E','E6.NE','E6.E','F6.NE','G6.NW'],
        # Rhine
        [ 'F7.W','F7.NW','F7.NE','G8.NW','G8.NE','H8.NW','H8.NE',
          'I8.NW','I8.NE','J7.NW','J7.NE','K8.NW','K8.NE','L8.NW',
          'L8.W','L9.NW','L9.NE','M10.NW','M10.NE'],
        # North of Stuttgart
        [ 'K8.NE','L7.NW','L7.NE','L7.E'],
        # Moser
        ['I8.W','I8.SW' ,'I8.SE','J9.NW','J9.NE','J9.E'],
        ['J9.NW','J9.W','J9.SW','J9.SE'],
        # Danube
        [ 'S1.E','S1.SE','S1.SW','R1.SE','R1.SW','Q2.SE','Q3.E',
          'Q3.SE','Q3.SW','P3.SE','P3.SW','O4.SE','O4.SW','N4.SE',
          'N4.SW','N5.W','N5.SW','N6.W','N6.SW','N7.W','N7.SW','N8.W'],
        # Inn
        [ 'O5.NE','O5.E','O5.SE','O6.E','P6.NE','P6.E'],
        # Meuse
        ['E7.SW','F7.W','F7.SW','F7.SE','F8.E','F8.SE','G9.SW',
         'G9.SE','H9.SW','H9.SE','I10.SW','I10.SE','J10.SW'],
        # Sampre
        ['F9.E','F9.SE','F10.E','F10.SE'],
        # Shelde
        ['E9.SW','E9.SE','E10.E','E10.SE' ,'E11.E','E11.SE'],
        # Somme
        ['D12.SE','E13.NW','E13.NE','F12.NW','F12.NE','G12.NW'],
        # Aisne
        ['F13.E','F13.NE','G13.NW','G13.NE','H12.NW',
         'H11.W','H11.NW','H11.NE'],
        # Marne
        ['I12.SW','I12.W','I12.NW','I12.NE'],
        # Aube
        ['J12.W','J12.NW','J12.NE'],
        # Doubs
        ['M12.NW','M12.W','M12.SW','M13.W','M13.SW','M13.SE',
         'N13.SW','N13.SE'],
        # Rhone
        ['N13.NE','N13.E','N13.SE','N14.E','N14.SE','O15.SW','O15.SE'
         ,'P15.SW','P15.SE','Q16.SW','Q16.SE','Q17.E'],
        ['Q16.SW','Q17.W','Q17.SW'],
        ['Q16.W','Q16.NW','Q16.NE','Q15.E','Q15.NE'],
        # Aude
        ['Q19.NW','Q19.W','P19.NW','O19.SE','O20.E'],
        # Tran
        ['P18.NE','P18.NW','O18.NE','O18.NW','O18.W','O18.SW',
         'N18.SE','N18.SW','N19.W'],
        # Lot
        [ 'M19.SW','M19.W','M19.NW','M19.NE','M18.E','M18.NE',
          'M17.E','N16.SE','N16.E'],
        # Garonne
        ['J19.W','J19.SW','J19.SE','J19.E','K20.NE','K20.E','L19.SE',
         'L19.E','M20.NE','N20.NW','N20.NE','N20.E'],
        # Loire
        [ 'G17.SW','G17.W','G17.NW'],
        ['G19.W','G19.NW','G18.W','G18.NW','G18.NE','G17.E','G17.NE',
         'H16.NW','H16.NE','H15.E','H15.NE','I15.NW','I15.NE','J14.NW',
         'J14.NE','K15.NW','K15.NE','L14.NW','L14.NE','M15.NW','M15.NE',
         'M15.E','M15.SE','N16.NW','N16.NE'],
        # Creuse
        [ 'H17.NW','H17.NE','I17.NW','I17.NE','J16.NW','J16.NE',
          'K17.NW','K17.NE'],
        # Vienne
        [ 'H17.NE','I18.NW','I18.NE','I18.E', 'J18.NE'],
        # Vilainne
        [ 'E19.E','E19.NE','E18.E','E18.NE'],
        # Selue
        [ 'E17.W','E17.NW','E17.NE'],
        # Below Le HAvre
        [ 'D15.NE','E16.NW','E16.NE'],
        # Seine
        ['E14.SW','E14.SE','E14.E','F13.SE','F13.E','G13.SE',
         'H12.SW','H12.SE','I12.SW','I12.SE','J12.SW','J12.SE'
         ],
    ]
    edges = {
        ('NW','NE'): ('N',  lambda c,r:(c,r-1)),
        ('NE','E'):  ('NE', lambda c,r:(c+1,r-(c%2))),
        ('E','SE'):  ('SE', lambda c,r:(c+1,r+((c+1)%2))),
        ('SE','SW'): ('S',  lambda c,r:(c,r+1)),
        ('SW','W'):  ('SW', lambda c,r:(c-1,r+((c+1)%2))),
        ('W','NW'):  ('NW', lambda c,r:(c-1,r-(c%2)))}
    samecol = {
        'NW': 'SW',
        'NE': 'SE',
        'SW': 'NW',
        'SE': 'NE' }

    def getEdge(w1,w2):
        return edges.get((w1,w2),edges.get((w2,w1),None))
    def getTarget(edge,c1,r1):
        if edge is None:
            return ''
        targ = edge[1](c1,r1)
        if targ is None:
            return ''
        return hexName(targ[0],targ[1])
    def hexName(c,r):
        return f'{chr(c+ord("A")-1):1s}{r}'

    from re import match

    #foo = open('crossings.tex','w')
    #print(r'\begin{scope}[line width=2pt,red,'
    #      f'dash pattern=on 0mm off 5mm on 10mm off 5mm]',file=foo)
    
    crossing = []
    for river in rivers:
        for v1,v2 in zip(river[:-1],river[1:]):
            # print(f'{v1:6s}-{v2:6s} -> ',end='')
            h1 = v1[:v1.index('.')]
            h2 = v2[:v2.index('.')]
            s1 = match(r'([A-Z])([0-9]+)',h1)
            s2 = match(r'([A-Z])([0-9]+)',h2)
            c1 = ord(s1[1])-ord('A')+1
            c2 = ord(s2[1])-ord('A')+1
            r1 = int(s1[2])
            r2 = int(s2[2])
            w1 = v1[v1.index('.')+1:]
            w2 = v2[v2.index('.')+1:]
            edge = ['?','?']
            targ = '?'
            orig = '?'
            if h1 == h2:
                edge = getEdge(w1,w2)
                if edge is None:
                    raise RuntimeError(f'Edge {w1},{w2} not found')

                orig = h1
                targ = getTarget(edge,c1,r1)
            else:
                if c1 == c2: # Same column
                    ww2   = samecol.get(w2,None)
                    # print(f'{v1:6s} - {v2:6s} -> {w2}')
                    if ww2 is None:
                        h1 = h2
                        c1, c2 = c2, c1
                        r1, r2 = r2, r1
                        w1, w2 = w2, w1
                        ww2 = samecol[w2]
                    if ww2 is None:
                        raise RuntimeError(f'Same vertex {w2} not found')
                    w2 = ww2
                    edge = getEdge(w1,w2)
                    orig = h1
                    targ = getTarget(edge,c1,r1)
                else:
                    # Make sure we only look right 
                    if c2 < c1:
                        c1, c2 = c2, c1
                        r1, r2 = r2, r1
                        w1, w2 = w2, w1
                        h1, h2 = h2, h1
                        #print(f'<> {h1+"."+w1}-{h2+"."+w2} ', end='')

                    assert w1 != 'W',\
                        f'Invalid first corner {w1}'
                    assert w2 != 'E',\
                        f'Invalid second corner {w2}'

                    # Only take top edge 
                    if w1 in ['SE','SW']:
                        w1 =  samecol[w1]
                        r1 += 1
                        # print(f'[1 -> {hexName(c1,r1)+"."+w1:6s}] ',end=' ')

                    if w2 in ['SE','SW']:
                        w2 =  samecol[w2]
                        r2 += 1
                        # print(f'[2 -> {hexName(c2,r2)+"."+w2:6s}] ',end='')

                    # print(f'({hexName(c1,r1)+"."+w1}-'
                    #       f'{hexName(c2,r2)+"."+w2}) -> ',end='')
                    cc = c2
                    rr = r2
                    if w1 == 'NE':
                        w1  =  'W'
                        if w2 == 'NW' and (c1 % 2 == 0) and r1 != r2:
                            rr -= 1
                            w2 =  samecol[w2]
                        if w2 == 'NW' and (c1 % 2 == 1) and r1 == r2:
                            rr -= 1
                            w2 =  samecol[w2]
                        
                    elif w1 == 'E':
                        w1   = 'NW'
                    elif w1 == 'NW':
                        assert w2 == 'W',\
                            f'w1={w1} but w2={w2}'
                        cc   = c1
                        rr   = r1
                        w2   = 'NE'

                    # print(f'{hexName(cc,rr)} {w1:2s}-{w2:2s} ->',end='')
                    edge = getEdge(w1,w2)
                    orig = hexName(cc,rr)
                    targ = getTarget(edge,cc,rr)
                        
            crossing.append([orig,targ])
            # print(f'Crossing {orig:3s} -> {targ:3s} ({edge[0]})')
            # print(fr'  \draw[->] ({orig})--({targ});'
            #       f' % {v1:6s}--{v2:6s}',file=foo)

    # print(r'\end{scope}',file=foo)
    # foo.close()
    
    return ':'+':'.join([f'{h1}{h2}:{h2}{h1}' for h1,h2 in crossing])

# --------------------------------------------------------------------
def addTurnPrototypes(n,
                      turns,
                      main,
                      prototypesContainer,
                      phaseNames,
                      marker='game turn',
                      zoneName='Turn track'):
    # ----------------------------------------------------------------
    # Loop over turns, and create prototype for game turn marker to
    # move it along the turn track.  We do that by adding commands to
    # the turn track, which then delegates to the prototype.
    #
    # We add the prototypes to the game turn marker later on.
    board = main['mapName']
    turnp = []
    for t in range(1,n+1):
        k = key(NONE,0)+f',Turn{t}'
        turns.addHotkey(hotkey       = k,
                        match        = (f'{{Turn=={t}&&'
                                        f'Phase=="{phaseNames[0]}"}}'),
                        reportFormat = f'=== <b>Turn {t}</b> ===',
                        name         = f'Turn {t}')
        main.addMassKey(name         = f'Turn marker to {t}',
                        buttonHotkey = k,
                        hotkey       = k,
                        buttonText   = '', 
                        target       = '',
                        filter       = (f'{{BasicName == "{marker}"}}'))
        pn     = f'To turn {t}'
        traits = [
            SendtoTrait(mapName     = board,
                        boardName   = board,
                        name        = '',
                        restoreName = '',
                        restoreKey  = '',
                        zone        = zoneName,
                        destination = 'R', #R for region
                        region      = (f'{marker}' if t == 1 else f'Turn {t}'),
                        key         = k,
                        x           = 0,
                        y           = 0),
            BasicTrait()]
        prototypesContainer.addPrototype(name        = f'{pn} prototype',
                                         description = f'{pn} prototype',
                                         traits      = traits)
        turnp.append(pn)

    return turnp

# --------------------------------------------------------------------
# Our main patching function
def patch(build,data,vmod,gverbose=False):
    from re  import sub
    from PIL import Image
    from io  import BytesIO

    is44 = '44' in vmod.fileName()
    game = build.getGame()

    hexes = {
        'A10': 'britain',
        'A11': 'britain',
        'A12': 'britain',
        'A13': 'britain',
        'A14': ('britain','city',1, 'a'), # % Southampton
        'A15': 'britain',
        ## b                                
        'B10': 'britain',
        'B11': 'britain',
        'B12': ('britain','city',1, 'a'), # # London
        'B13': 'britain',
        'B14': 'britain',
        'B18': '',
        'B19': 'fort',# # Brest
        ## c                                
        'C7': 'swamp',
        'C12': ('britain','city',1, 'a'), # # Dover
        'C13': 'britain',
        'C16': ('fort',1, 'a'), # # Cherbourg
        'C18': '',
        'C19': 'woods',
        'C20': '',
        ## d                                
        'D2': '',
        'D4': '',
        'D5': '',
        'D6': 'swamp',
        'D7': ('fort',1, 'a'), # # Amsterdam
        'D8': 'swamp',
        'D10': 'fort', # # Dunkirk
        'D11': 'fort', # # Calais
        'D12': '',
        'D14': ('fort',1, 'a'), # # Le Havre
        'D15': 'woods',
        'D16': 'woods',
        'D17': '',
        'D18': 'woods',
        'D19': ('fort',1, 'a'), # # Lorient
        ## e                                
        'E1': '',
        'E2': '',
        'E3': ('city',1, 'g'), # # Hamburg
        'E4': 'fort', # # Wilhelmshaven
        'E5': '',
        'E6': 'swamp',
        'E7': 'swamp',
        'E8': '',
        'E9': ('fort',4, 'a'), # # Antwerp
        'E10': '',
        'E11': 'city', # # Lille
        'E12': '',
        'E13': '',
        'E14': 'fort', # # Dieppe
        'E15': '',
        'E16': 'woods',
        'E17': '',
        'E18': '',
        'E19': '',
        ## f                                
        'F1': '',
        'F2': '',
        'F3': 'woods',
        'F4': '',
        'F5': '',
        'F6': '',
        'F7': '',
        'F8': '',
        'F9': 'city',
        'F10': '',
        'F11': '',
        'F12': '',
        'F13': '',
        'F14': '',
        'F15': '',
        'F16': '',
        'F17': '',
        'F18': 'city', # # Nantes
        ## g                                
        'G1': '',
        'G2': '',
        'G3': 'woods',
        'G4': '',
        'G5': '',
        'G6': '',
        'G7': ('city',2, 'g'), # # Essen
        'G8': '',
        'G9': '',
        'G10': '',
        'G11': 'woods',
        'G12': '',
        'G13': ('fort',1, 'b'), # # Paris
        'G14': '',
        'G15': '',
        'G16': 'city', # # Le Mans
        'G17': '',
        'G18': '',
        'G19': 'swamp',
        ## h                                
        'H1': '',
        'H2': '',
        'H3': '',
        'H4': '',
        'H5': 'woods',
        'H6': 'woods',
        'H7': 'fort', # # Cologne
        'H8': 'woods',
        'H9': 'woods',
        'H10': 'woods',
        'H11': '',
        'H12': '',
        'H13': '',
        'H14': 'city', # # Orleans
        'H15': '',
        'H16': '',
        'H17': '',
        'H18': '',
        'H19': 'swamp',
        ## i                                
        'I1': ('fort',1, 'g'), # # Berlin# berlin
        'I2': '',
        'I3': '',
        'I4': 'woods',
        'I5': '',
        'I6': 'woods',
        'I7': '',
        'I8': ('woods','fortified'),
        'I9': ('woods','fortified'),
        'I10': 'woods',
        'I11': 'woods',
        'I12': '',
        'I13': '',
        'I14': 'woods',
        'I15': '',
        'I16': '',
        'I17': '',
        'I18': '',
        'I19': '',
        'I20': '',
        ## j                                
        'J1': '',
        'J2': '',
        'J3': '',
        'J4': '',
        'J5': '',
        'J6': 'city', # # Frankfurt
        'J7': '',
        'J8': ('woods','fortified'),
        'J9': 'woods',
        'J10': 'fort', # # Nancy
        'J11': '',
        'J12': 'woods',
        'J13': '',
        'J14': 'woods',
        'J15': '',
        'J16': '',
        'J17': '',
        'J18': 'woods',
        'J19': ('city',1, 'a'), # # Bordeaux
        'J20': 'woods',
        ## k                                
        'K1': '',
        'K2': '',
        'K3': 'city', # # Leipzig
        'K4': 'woods',
        'K5': '',
        'K6': 'woods',
        'K7': 'woods',
        'K8': 'fortified',
        'K9': 'woods',
        'K10': '',
        'K11': '',
        'K12': 'woods',
        'K13': 'woods',
        'K14': '',
        'K15': 'woods',
        'K16': '',
        'K17': '',
        'K18': '',
        'K19': 'woods',
        'K20': 'woods',
        ## l                                
        'L1': '',
        'L2': 'mountains',
        'L3': 'mountains',
        'L4': '',
        'L5': '',
        'L6': 'woods',
        'L7': 'city', # # Stuttgart
        'L8': ('mountains','fortified'),
        'L9': 'fort', # # Strasbourg
        'L10': 'mountains',
        'L11': 'mountains',
        'L12': '',
        'L13': 'woods',
        'L14': '',
        'L15': 'city', # # Vichy
        'L16': 'mountains',
        'L17': '',
        'L18': 'woods',
        'L19': '',
        'L20': 'woods',
        ## m                                
        'M1': 'mountains',
        'M2': 'mountains',
        'M3': 'mountains',
        'M4': 'woods',
        'M5': 'mountains',
        'M6': 'woods',
        'M7': 'mountains',
        'M8': 'mountains',
        'M9': ('mountains','fortified'),
        'M10': 'mountains',
        'M12': 'mountains',
        'M13': 'mountains',
        'M14': 'mountains',
        'M15': '',
        'M16': 'mountains',
        'M17': 'mountains',
        'M18': '',
        'M19': '',
        'M20': '',
        ## n                                
        'N1': 'fort', # # Prague
        'N2': 'woods',
        'N3': 'woods',
        'N4': 'woods',
        'N5': 'woods',
        'N6': '',
        'N7': '',
        'N8': '',
        'N13': 'woods',
        'N14': '',
        'N15': 'mountains',
        'N16': 'mountains',
        'N17': '',
        'N18': '',
        'N19': 'city', # # Toulouse
        'N20': 'woods',
        ## o                                
        'O1': '',
        'O2': '',
        'O3': 'woods',
        'O4': '',
        'O5': '',
        'O6': 'city', # # Munich
        'O7': 'woods',
        'O8': 'mountains',
        'O13': 'mountains',
        'O14': 'mountains',
        'O15': '',
        'O16': 'mountains',
        'O17': 'mountains',
        'O18': 'mountains',
        'O19': 'mountains',
        'O20': 'woods',
        ## p                                
        'P1': '',
        'P2': 'woods',
        'P3': 'mountains',
        'P4': 'mountains',
        'P5': 'woods',
        'P6': 'woods',
        'P7': 'mountains',
        'P13': 'mountains',
        'P14': 'mountains',
        'P15': '',
        'P16': '',
        'P17': '',
        'P18': '',
        'P19': 'woods',
        ## q                                
        'Q1': '',
        'Q2': 'mountains',
        'Q3': 'mountains',
        'Q4': '',
        'Q5': 'mountains',
        'Q6': 'mountains',
        'Q14': 'mountains',
        'Q15': 'mountains',
        'Q16': '',
        'Q17': 'swamp',
        'Q19': 'mountains',
        ## r                                
        'R1': 'fort', # # Vienna
        'R2': 'woods',
        'R3': 'woods',
        'R4': 'mountains',
        'R5': 'mountains',
        'R14': 'mountains',
        'R15': 'woods',
        'R16': ('city',1, 'a'), # # Marseilles
        ## S                                
        'S1': '',
        'S2': '',
        'S3': 'woods',
        'S4': 'mountains',
        'S5': 'mountains',
        'S14': 'mountains',
        'S15': 'woods',
        'S16': 'woods',
        ## T
        'T1': '',
        'T2': '',
        'T3': 'woods',
        'T4': 'woods',
        'T5': 'woods',
        'T19': ('city',1, 'a'),
    }
    supplies = {}
    for hex, hdata in hexes.items():
        if not isinstance(hdata,tuple):
            continue
        if 'city' not in hdata and 'fort' not in hdata:
            continue 
        mp = hdata[-2]
        fc = hdata[-1]

        supplies[hex] = [mp,
                         1 if fc=='a' else
                         2 if fc=='g' else
                         3]

    # ================================================================
    #
    # Global properties
    #
    # ----------------------------------------------------------------
    gp                = game.getGlobalProperties()[0];
    # ----------------------------------------------------------------
    #
    # Lists of hexes and crossings
    #
    portHexes         = getPortHexes     ()
    swampHexes        = getSwampHexes    (hexes)
    woodsHexes        = getWoodsHexes    (hexes)
    fortifiedHexes    = getFortifiedHexes(hexes)
    cityHexes         = getCityHexes     (hexes)
    mountainsHexes    = getMountainsHexes(hexes)
    fortHexes         = getFortHexes     (hexes)
    britainHexes      = getBritainHexes  (hexes)
    continentHexes    = getContinentHexes(hexes)
    invasionsCrossing = getInvasionCrossings()
    riverCrossing     = getRiverCrossings()
    globalPort        = 'Port'
    globalSwamp       = 'Swamp'
    globalWoods       = 'Woods'
    globalFortified   = 'Fortified'
    globalCity        = 'City'
    globalMountains   = 'Mountains'
    globalFort        = 'Fort'
    globalBritain     = 'Britain'
    globalContinent   = 'Continent'
    globalInvasions   = 'Invasions'
    globalRivers      = 'Rivers'
    for h,n in [[portHexes,         globalPort     ],   
                [swampHexes,        globalSwamp    ],    
                [woodsHexes,        globalWoods    ],   
                [fortifiedHexes,    globalFortified],   
                [cityHexes,         globalCity     ],   
                [mountainsHexes,    globalMountains],   
                [fortHexes,         globalFort     ],   
                [britainHexes,      globalBritain  ],   
                [continentHexes,    globalContinent],   
                [invasionsCrossing, globalInvasions],   
                [riverCrossing,     globalRivers   ]]:
        gp.addProperty(name         = n,
                       initialValue = h,
                       description  = f'List of {n} hexes')

                         
    # ----------------------------------------------------------------
    globalMP        = 'GlobalMP'
    globalDeclare   = 'NotDeclare'
    globalResolve   = 'NotResolve'
    globalInitDE    = 'NotInitDE'
    globalInitAL    = 'NotInitAL'
    globalWeather   = 'BadWeather'
    defenderHex     = 'DefenderHex'
    globalCoastal   = 'Coastal'
    globalBomber    = 'Bombing'
    globalNormal    = 'NormalDice'
    globalBiased    = 'BiasedDice'
    isTutorial      = 'IsTutorial'
    gp              = game.getGlobalProperties()[0];
    gp.addProperty(name         = defenderHex,
                   initialValue = '',
                   description  = 'Hex of defending unit')
    gp.addProperty(name         = globalCoastal,
                   initialValue = 'true',
                   description  = 'Invasion over edge') 
    gp.addProperty(name         = defenderHex,
                   initialValue = '',
                   description  = 'Defended hex')
    gp.addProperty(name         = globalDeclare,
                   initialValue = 'true',
                   description  = 'True when not in declare phase')
    gp.addProperty(name         = globalResolve,
                   initialValue = 'true',
                   description  = 'True when not in combat phase')
    gp.addProperty(name         = globalInitDE,
                   initialValue = 'false',
                   description  = 'True when not in initial wheather phase')
    gp.addProperty(name         = globalInitAL,
                   initialValue = 'true',
                   description  = 'True when not in initial invasion phase')
    gp.addProperty(name         = globalWeather,
                   initialValue = 'false',
                   description  = 'True when bad weather')
    gp.addProperty(name         = globalBomber,
                   initialValue = 'false',
                   description  = 'True resolving bombing mission')
    gp.addProperty(name         = globalMP,
                   initialValue = 0,
                   description  = 'Counter of factions MPs')
    gp.addProperty(name         = isTutorial,
                   initialValue = str(doTutorial).lower(), 
                   description  = 'Is tutorial loaded')
    gp.addProperty(name         = globalNormal,
                   initialValue = str(False).lower(), 
                   description  = 'If true, disable normal dice')
    gp.addProperty(name         = globalBiased,
                   initialValue = str(True).lower(), 
                   description  = 'Is true, disable biased dice')
   
    # ================================================================
    #
    # Options
    #
    # ----------------------------------------------------------------
    optInitiative   = 'optInitiative'
    optFightOn      = 'optFightOn'
    optMovePoints   = 'optMovePoints'
    optReplacements = 'optReplacements'
    optWeather      = 'optWeather'
    optReaction     = 'optReaction'
    optBomber       = 'optBomber'
    optCAS          = 'optCAS'
    optAirborne     = 'optAirborne'
    optAA           = 'optAA'
    optNational     = 'optNational'
    optPursue       = 'optPursue'
    optExtraEffort  = 'optExtraEffort'
    optMulberry     = 'optMulberry'
    optIsolated     = 'optIsolated'
    optGarrison     = 'optGarrison'
    for o,t in [[optInitiative  , 'Initiative'],
		[optFightOn     , 'Fight-on'],
		[optMovePoints  , 'Alternative move points'],
		[optReplacements, 'Replacements'],
		[optWeather     , 'Weather'],
		[optReaction    , 'Air unit reactions'],
		[optBomber      , 'Allied strategic bombing'],
		[optCAS         , 'Close Air Support'],
		[optAirborne    , 'Airborne drops'],
		[optAA          , 'Anti-air fire'],
		[optNational    , 'National integrity'],
		[optPursue      , 'Pursuit'],
		[optExtraEffort , 'Extra effort'],
		[optMulberry    , 'Mulberry harbours'],
		[optIsolated    , 'Step on out-of-supply'],
		[optGarrison    , 'Garrisons']]:
        gp.addProperty(name         = o,
                       initialValue = 'true',
                       description  = t)

    # ================================================================
    #
    # Keys, global properties
    #
    # ----------------------------------------------------------------
    updateHex      = key(NONE,0)+',updateHex'
    updateHQ       = key(NONE,0)+',updateHQ'
    setHQHex       = key(NONE,0)+',setHQHex'
    updateCoastal  = key(NONE,0)+',updateCoastal'
    updateBomber   = key(NONE,0)+',updateBomber'
    resetCoastal   = key(NONE,0)+',resetCoastal'
    checkDeclare   = key(NONE,0)+',checkDeclare'
    checkResolve   = key(NONE,0)+',checkResolve'
    checkInitDE    = key(NONE,0)+',checkInitDE'
    checkInitAL    = key(NONE,0)+',checkInitAL'
    calcOddsKey    = key(NONE,0)+',wgCalcBattleOdds'
    calcFracKey    = key(NONE,0)+',wgCalcBattleFrac'
    calcAF         = key(NONE,0)+',wgCalcBattleAF'
    calcDF         = key(NONE,0)+',wgCalcBattleDF'
    noTerrainKey   = key(NONE,0)+',noTerrain'
    noTerrainClear = key(NONE,0)+',resetNoTerrain'
    dieRoll        = key(NONE,0)+',dieRoll'
    setWeather     = key(NONE,0)+',setWeather'
    rollWeather    = key(NONE,0)+',rollWeather'
    flipTurn       = key(NONE,0)+',flipTurn'
    startAllied    = key(NONE,0)+',startAllied'    
    startGerman    = key(NONE,0)+',startGerman'
    startInitiative= key(NONE,0)+',startInitiative'
    resolveKey     = key('Y')
    declareKey     = key('X')
    toggleKey      = key(NONE,0)+',toggleOpt'
    flipKey        = key(NONE,0)+',flipOpt'
    setOptional    = key(NONE,0)+',setOpt'
    cleanOptional  = key(NONE,0)+',cleanOpt'
    deleteKey      = key(NONE,0)+',delete'
    optKey         = key('Q',ALT)
    optShow        = key(NONE,0)+',optShow'
    incrMPKey      = key(NONE,0)+',incrMP'
    decrMPKey      = key(NONE,0)+',decrMP'
    turnMPKey      = key(NONE,0)+',turnMP'
    updateMPKey    = key(NONE,0)+',updateMP'
    fixedMPKey     = key(NONE,0)+',fixedMP'
    varMPKey       = key(NONE,0)+',varMP'
    resetMPKey     = key(NONE,0)+',resetMP'
    giveMP         = key(NONE,0)+',giveMP'
    setGerman      = key(NONE,0)+',setGerman'
    setAllied      = key(NONE,0)+',setAllied'
    setNone        = key(NONE,0)+',setNone'
    nextPhase      = key('T',ALT) # key(NONE,0)+',nextPhase'
    resetStep      = key(NONE,0)+',resetStep'
    toggleStep     = key(NONE,0)+',toggleStep'
    toggleBias     = key(NONE,0)+',toggleBias'
    stepKey        = key('F')
    eliminateKey   = key('E')
    eliminateCmd   = key(NONE,0)+',eliminate'
    resetIsolated  = key(NONE,0)+',resetIsolated'
    stepIsolated   = key(NONE,0)+',stepIsolated'
    isolatedTwice  = key(NONE,0)+',isolatedTwice'
    stepOrElim     = key(NONE,0)+',stepOrElim'
    isolatedKey    = key('I')
    insupplyKey    = key('I',CTRL_SHIFT)
    restoreKey     = key('R')
    tutorialKey    = key(NONE,0)+',isTutorial'
    debug          = 'wgDebug'
    verbose        = 'wgVerbose'
    hidden         = 'wg hidden unit'
    currentBattle  = 'wgCurrentBattle'
    battleUnit     = 'wgBattleUnit'
    battleCalc     = 'wgBattleCalc'
    battleMarker   = 'wgBattleMarker'
    battleCtrl     = 'wgBattleCtlr'
    battleNo       = 'wgBattleNo'
    battleResult   = 'wgBattleResult'
    battleShift    = 'wgBattleShift'
    battleFrac     = 'wgBattleFrac'
    battleAF       = 'wgBattleAF'
    battleDF       = 'wgBattleDF'
    bomberPrototype= 'Allied air prototype'
    stackDx        = 8 # 'wgStackDx'
    stackDy        = 12 # 'wgStackDy'
    
    # ================================================================
    #
    # Default preferences
    #
    go    = game.getGlobalOptions()[0]
    prefs = go.getPreferences()
    prefs[debug]          ['default'] = False
    prefs[verbose]        ['default'] = False
    prefs['wgAutoOdds']   ['default'] = True
    prefs['wgAutoResults']['default'] = True
    # go.addIntPreference(name = stackDx, default = 3,
    #                     desc = 'Horizontal stacking offset when not expanded',
    #                     tab  = game['name'])
    # go.addIntPreference(name = stackDy, default = 3,
    #                     desc = 'Vertical stacking offset when not expanded',
    #                     tab  = game['name'])
    # Color for select rgb=(255,188,79) HTML=FFBC4F
    # ================================================================
    #
    # Global properties to store HQ locations
    #
    armies = {'Allied': ['al 1ab arhq',
                         'ca 1 arhq',
                         'fr 1 arhq',
                         'us 1 arhq',
                         'br 2 arhq',
                         'us 3 arhq',
                         'us 7 arhq',
                         'us 9 arhq',
                         'us 15 arhq'],
              'German': ['de 1 arhq',
                         'de 1ab arhq',
                         'de 5pz arhq',
                         'de 6pz arhq',
                         'de 7 arhq',
                         'de 11 arhq',
                         'de 15 arhq',
                         'de 19 arhq',
                         'de 25 arhq'] }
    for armys in armies.values():
        for army in armys:
            gname = army.replace(' ','_')
            gp.addProperty(name         = gname,
                           initialValue = '',
                           description  = f'Location of {army}')
            
    # ================================================================
    #
    # Inventory
    #
    # ----------------------------------------------------------------
    # Add an inventory sorted on the unit supply and isolation status. 
    filt = '{Faction=="Allied"||Faction=="German"}'
    grp  = 'Faction,Step_Level,Isolated_Level'
    disp = ('{PropertyValue==Faction ? Faction : '
            '(Step_Level==1 ? "Full" : "Reduced")+" "+'
            '(Isolated_Level==1 ? "In supply" : '
            'Isolated_Level==2 ? "Isolated" : '
            'Isolated_Level==3 ? "Twice isolated" : "")}')
    game.addInventory(include       = filt,
                      groupBy       = grp,
                      sortFormat    = '$PieceName$',
                      tooltip       = 'Show supply status of pieces',
                      nonLeafFormat = disp,
                      zoomOn        = True,
                      hotkey        = key('S',ALT),
                      refreshHotkey = key('S',ALT_SHIFT),
                      icon          = 'isolated-icon.png')
        
    # ================================================================
    #
    # Dice
    #
    # ----------------------------------------------------------------
    # Dice
    diceName  = '1d10Dice'
    dicesName = '2d10'
    diceKey   = key('0',ALT)
    dicesKey  = key('2',ALT)
    # dices = game.getDiceButtons()
    # dice  = dices.get('1d6',None)
    # dice['name']    = diceName
    # dice['icon']    = 'd10-icon.png'
    # dice['text']    = diceName
    # dice['tooltip'] = 'Roll d10'
    # dice['nSides']  = 10
    # dice['hotkey']  = diceKey
    # dices = game.addDiceButton(name         = dicesName,
    #                            hotkey       = dicesKey,
    #                            icon         = '2d10-icon.png',
    #                            nDice        = 2,
    #                            nSides       = 10,
    #                            text         = dicesName,
    #                            tooltip      = 'Roll 2d10',
    #                            reportFormat = ('** $name$ -> '
    #                                            'attacker=$result1$ '
    #                                            'defender=$result2$ '))
    dices = game.getSymbolicDices()
    for dn, dice in dices.items():
        if dn == '1d10Dice':
            dice['hotkey'] = diceKey
        else:
            dice['hotkey'] = dicesKey

    game.getPieceWindows()['Counters']['icon'] = 'unit-icon.png'
    
    # ----------------------------------------------------------------
    prototypeContainer = game.getPrototypes()[0]
    # ----------------------------------------------------------------
    maps = game.getMaps()
    main = maps['Board']

    # ----------------------------------------------------------------
    # Delete key
    dkey = main.getMassKeys().get('Delete',None)
    if dkey is not None:
        dkey['icon'] = ''

    # ----------------------------------------------------------------
    # Add layers to the map
    pieceLayer      = 'PieceLayer'
    unitLayer       = 'Units'
    btlLayer        = 'Units' #'Battle' - doesn't work with auto odds
                              #if not same as units - anyways, the
                              #markers should stack with units.
    oddLayer        = 'Odds'
    resLayer        = 'Result'
    objLayer        = 'Objectives'
    layerNames = {objLayer:  {'t': objLayer,  'i': ''},
                  unitLayer: {'t': unitLayer, 'i': '' },
                  #btlLayer,
                  oddLayer: {'t': oddLayer+' markers', 'i': ''},
                  resLayer: {'t': resLayer+' markers', 'i': ''} }
                  
    layers = main.addLayers(description='Layers',
                            layerOrder=layerNames)
    for lt,ln in layerNames.items():
        layers.addControl(name       = lt,
                          tooltip    = f'Toggle display of {ln}',
                          text       = f'Toggle {ln["t"]}',
                          icon       = ln['i'],
                          layers     = [lt])
    layers.addControl(name    = 'Show all',
                      tooltip = f'Show all',
                      text    = f'Show all',
                      command = LayerControl.ENABLE,
                      layers  = list(layerNames.keys()))

    lmenu = main.addMenu(description = 'Toggle layers',
                         text        = '',
                         tooltip     = 'Toggle display of layers',
                         icon        = 'layer-icon.png',
                         menuItems   = ([f'Toggle {ln["t"]}'
                                         for ln in layerNames.values()]
                                        +['Show all']))
    hideP  = main.getHidePiecesButton()
    if len(hideP) > 0:
        print('Found hide pieces button, moving layers before that')
        main.remove(layers)
        main.remove(lmenu)
        main.insertBefore(layers,hideP[0])
        main.insertBefore(lmenu, hideP[0])
    else:
        print('Hide pueces button not found',hideP)

    # ----------------------------------------------------------------
    # Change stacking to be more tight since stacks may grow high
    stackmetrics = main.getStackMetrics(single=True)[0]
    stackmetrics['unexSepX'] = stackDx
    stackmetrics['unexSepY'] = stackDy
    stackmetrics['exSepX']   = int(1.5*stackDx)
    stackmetrics['exSepY']   = int(1.5*stackDy)
    main['color']            = rgb(255,215,79)
    
    # ----------------------------------------------------------------
    # Extra documentation 
    doc  = game.getDocumentation()[0]
    doc.addHelpFile(title='More information',fileName='help/more.html')
    vmod.addFile('help/more.html', moreHelp.format(title=game['name']))

    # ================================================================
    #
    # Boards, etc.
    #
    # ----------------------------------------------------------------
    # Get the restore global key, set icon and remove the deadmap map,
    # since we will move eliminated units to the OOBs instead.
    unelimKey               = key('E',CTRL_SHIFT)
    restore                 = maps['DeadMap'].getMassKeys()['Restore']
    restore['icon']         = 'restore-icon.png'
    restore['hotkey']       = unelimKey
    restore['buttonHotkey'] = unelimKey
    oobs                    = game.getChartWindows()['OOBs']
    oobs['icon']            = 'oob-icon.png'
    game.remove(maps['DeadMap'])
    main.append(restore)

    # ----------------------------------------------------------------
    # Get optionals map
    opts = maps['Optionals']
    opts['allowMultiple'] = False
    opts['markMoved'] = 'Never'
    opts['hotkey']    = optKey
    opts['launch']    = True
    opts['icon']      = 'opt-icon.png'
    opts.remove(opts.getImageSaver()[0])
    opts.remove(opts.getTextSaver()[0])
    opts.remove(opts.getGlobalMap()[0])
    opts.remove(opts.getHidePiecesButton()[0])
    obrd = opts.getBoardPicker()[0].getBoards()['Optionals']
    # print(obrd['width'],obrd['height'])
    # obrd['width']  = 500
    # obrd['height'] = 800
    okeys = opts.getMassKeys()
    # print(okeys)
    for kn,k in okeys.items():
        # print(f'Removing {kn} from Optionals')
        opts.remove(k) 
    ostart = opts.getAtStarts(False)
    for at in ostart:
        # print(at['name'])
        if at['name'] == hidden:
            # print(f'Removing {at["name"]} from Optionals')
            opts.remove(at)

    game.addStartupMassKey(name        = 'Optionals',
                           hotkey      = optKey,
                           target      = '',
                           filter      = f'{{BasicName=="{hidden}"}}',
                           whenToApply = StartupMassKey.START_GAME,
                           reportFormat=
                           f'{{{debug}?("Show optionals window"):""}}')
    game.addStartupMassKey(name        = 'Tutorial',
                           hotkey      = tutorialKey,
                           target      = '',
                           filter      = f'{{BasicName=="{hidden}"}}',
                           whenToApply = StartupMassKey.START_GAME,
                           reportFormat=
                           f'{{{debug}?("Show notes window "+{isTutorial}):""}}'                           )
    
    # ----------------------------------------------------------------
    # Extract units on the OOB so we may remove units we do not need
    def stripAt(s):
        try:
            return s[:s.index('@')]
        except:
            return s
        
    oobbs           = {
        bn.replace(' OOB','') : b
        for mw in oobs.getMapWidgets().values()
        for wm in mw.getWidgetMaps().values()
        for bn,b in wm.getBoards().items()
    }
    oobu = {}
    for k, b in oobbs.items():
        l = []
        for zn,z in b.getZones().items():
            if 'oob' not in zn:
                continue
            for rg in z.getRegionGrids():
                l.extend([stripAt(s) for s in rg.getRegions().keys()])
        
        oobu[k] = l
    # pprint(oobu)
    
    # ----------------------------------------------------------------
    # Set custom icons on some global keys 
    mkeys = main.getMassKeys()
    mkeys['Eliminate']['icon'] = 'eliminate-icon.png'
    mkeys['Flip']     ['icon'] = 'flip-icon.png'

    # calcOddsAuto           = mkeys['Calc battle odds']
    # calcOddsAuto['filter'] = '{Phase.contains("combat")&&wgBattleCalc==true}'
    
    # ----------------------------------------------------------------
    # Get Zoned area
    zoned = main.getBoardPicker()[0].getBoards()['Board'].getZonedGrids()[0]

    # ----------------------------------------------------------------
    # Adjust the hexes
    #
    # Get the hex grid and turn of drawing
    zones  = zoned.getZones()
    hzone  = zones['hexes']
    hgrids = hzone.getHexGrids()
    hgrid  = hgrids[0]
    hnum   = hgrid.getNumbering()[0]
    hnum['hOff']  = 0


    # ================================================================
    #
    # Turn track
    #
    # ----------------------------------------------------------------
    turns         = game.getTurnTracks()['Turn']
    phaseNames    = ['Weather',                         # 0
                     'Allied move points',              # 1
                     'Allied reinforcements',           # 2
                     'Allied replacements',             # 3
                     'Allied port transfers',           # 4
                     'Allied invasions',                # 5
                     'Allied movement',                 # 6
                     'German air reactions',            # 7
                     'Allied strategic air missions',   # 8
                     'Allied combat',                   # 9
                     'Allied supply',                   # 10
                     'German move points',              # 11
                     'German reinforcements',           # 12
                     'German replacements',             # 13
                     'German movement',                 # 14
                     'Allied air reactions',            # 15
                     'German combat',                   # 16
                     'German supply']                   # 17
    germanNo       = phaseNames.index('German move points')
    alliedSup      = phaseNames.index('Allied supply')
    germanSup      = phaseNames.index('German supply')
    phases         = turns.getLists()['Phase']
    phases['list'] = ','.join(phaseNames)
    turns['reportFormat'] = '--- <b><i>$newTurn$</i></b> ---'

    # ----------------------------------------------------------------
    # Turn track progress 
    turnp = addTurnPrototypes(12,turns,main,prototypeContainer,phaseNames,
                              marker='game turn', zoneName='Turn track')

    # ----------------------------------------------------------------
    # Specific actions 
    turns.addHotkey(hotkey       = flipTurn,
                    match        = f'{{Phase=="{phaseNames[germanNo]}"}}',
                    reportFormat = '--- <b>German Turn</b> ---',
                    name         = 'German Turn')
    turns.addHotkey(hotkey       = flipTurn,
                    match        = f'{{Phase=="{phaseNames[0]}"}}',
                    reportFormat = '--- <b>Allied Turn</b> ---',
                    name         = 'Allied Turn')
    turns.addHotkey(hotkey       = rollWeather,
                    match        = f'{{Phase=="{phaseNames[0]}"}}',
                    reportFormat = (f'{{{verbose}?("--- <b>Weather</b>: <i>"+'
                                    f'({globalWeather}==true?"bad":"clear")+'
                                    f'"</i>---"):""}}'),
                    name         = 'Resolve weather')
    turns.addHotkey(hotkey       = cleanOptional,
                    match        = f'{{Phase=="{phaseNames[1]}"&&Turn==1}}',
                    reportFormat = (f'{{{debug}?"~Cleaning for optional rules":'
                                    f'""}}'),
                    name         = 'CleanOptional')
    turns.addHotkey(hotkey       = turnMPKey+'Allied',
                    match        = (f'{{Phase=="{phaseNames[1]}"}}'),
                    reportFormat = f'{{{debug}?"~ Increment Allied MP":""}}',
                    name         = 'CleanOptional')
    turns.addHotkey(hotkey       = turnMPKey+'German',
                    match        = (f'{{Phase=="{phaseNames[11]}"}}'),
                    reportFormat = f'{{{debug}?"~ Increment German MP":""}}',
                    name         = 'CleanOptional')
    turns.addHotkey(hotkey       = stepIsolated,
                    match        = (f'{{{optIsolated}&&'
                                    f'(Phase=="{phaseNames[11]}"||'
                                    f'Phase=="{phaseNames[0]}")}}'),
                    reportFormat = f'{{{debug}?("~ Step loss to isolated"):""}}',
                    name         = 'StepLossIsolated')
    turns.addHotkey(hotkey       = isolatedTwice,
                    match        = (f'{{{optIsolated}&&'
                                    f'(Phase=="{phaseNames[10]}"||'
                                    f'Phase=="{phaseNames[-1]}")}}'),
                    reportFormat = f'{{{debug}?("~ Twice isolated"):""}}',
                    name         = 'StepTwice')
    turns.addHotkey(hotkey       = startInitiative,
                    match        = (f'{{Turn==1&&Phase=="{phaseNames[1]}"}}'))
                    
    # --- Add commands to turn track to do stuff, and provide reminders
    turns.addHotkey(hotkey = checkDeclare,
                    name   = 'Check for (any) declare phase')
    turns.addHotkey(hotkey = checkResolve,
                    name   = 'Check for (any) combat phase')
    turns.addHotkey(hotkey = checkInitDE,
                    name   = 'Check for initial phase')
    turns.addHotkey(hotkey = checkInitAL,
                    name   = 'Check for initial phase')
    turns.addHotkey(hotkey = noTerrainClear,
                    name   = 'Reset no terrain flag',
                    match  = '{Phase=="Allied supply"}')    
    # --- Skip phases ------------------------------------------------
    skipPhases = {optReplacements: 'replacements',
                  optReaction:     'air reactions',
                  optBomber:       'strategic air missions'}
    for opt, phase in skipPhases.items():
        turns.addHotkey(hotkey = nextPhase,
                        name   = f'Skip {phase} if not {opt}',
                        match  = (f'{{{opt}==false&&'
                                  f'Phase.contains("{phase}")}}'))
    # --- Clear markers phases ---------------------------------------
    keys = turns.getHotkeys(asdict=True)
    keys['Clear battle markers']['match'] = (
        f'{{Phase=="German movement"'
        f'||Phase=="German supply"'
        f'||Phase=="Allied invasion"'
        f'||Phase=="Allied supply"'
        f'}}')

    # ================================================================
    #
    # Global key commands
    #
    # ----------------------------------------------------------------
    curBtl        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true}}')
    curDef        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true&&'
                     f'IsAttacker==false}}')
    curAtt        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true&&'
                     f'IsAttacker==true}}')
    keys                     = main.getMassKeys()
    userMark                 = keys['User mark battle']
    selMark                  = keys['Selected mark battle']
    resMark                  = keys['Selected resolve battle']
    userMark['canDisable']   = True
    userMark['propertyGate'] = globalDeclare
    selMark ['canDisable']   = True
    selMark ['propertyGate'] = globalDeclare
    resMark ['canDisable']   = True
    resMark ['propertyGate'] = globalResolve
    
    # ----------------------------------------------------------------
    # Add global key to set isolated status 
    main.addMassKey(name         = 'Add isolated marker',
                    buttonHotkey = isolatedKey,
                    hotkey       = isolatedKey,
                    buttonText   = '', # g['name']
                    icon         = 'isolated-icon.png',
                    reportSingle = True,
                    singleMap    = True,
                    tooltip      = 'Add isolated marker')
    main.addMassKey(name         = 'Isolated twice',
                    buttonHotkey = isolatedTwice,
                    hotkey       = isolatedKey,
                    icon         = '',
                    reportSingle = True,
                    singleMap    = True,
                    target       = '',
                    filter       = (f'{{((Phase=="{phaseNames[10]}"&&'
                                    f'Faction=="Allied")||'
                                    f'(Phase=="{phaseNames[-1]}"&&'
                                    f'Faction=="German"))&&'
                                    f'Isolated_Level==2}}'))
    main.addMassKey(name         = 'Step loss isolated',
                    buttonHotkey = stepIsolated,
                    hotkey       = stepOrElim,
                    icon         = '',
                    reportSingle = True,
                    singleMap    = True,
                    target       = '',
                    filter       = (f'{{((Phase=="{phaseNames[0]}"&&'
                                    f'Faction=="German")||'
                                    f'(Phase=="{phaseNames[11]}"&&'
                                    f'Faction=="Allied"))&&'
                                    f'Isolated_Level==3}}'))
    main.addMassKey(name         = 'Replace unit',
                    buttonHotkey = restoreKey,
                    hotkey       = restoreKey,
                    buttonText   = '', # g['name']
                    icon         = '',
                    reportSingle = True,
                    singleMap    = True)
    main.addMassKey(name         = 'In supply',
                    buttonHotkey = insupplyKey,
                    hotkey       = insupplyKey,
                    buttonText   = '', # g['name']
                    icon         = '',
                    reportSingle = True,
                    singleMap    = True)
    # ----------------------------------------------------------------
    # Add some global keys to the map
    #
    # - Flip turn marker (Ctrl+Shift+F)
    # - Mark twice out-of-supply Allied units (Ctrl+Shift+I)
    # - Eliminate twice isolated Allied units (Ctrl+Shift+E)
    main.addMassKey(name         = 'Flip turn marker',
                    buttonHotkey = flipTurn,
                    hotkey       = stepKey,
                    buttonText   = '', 
                    target       = '',
                    singleMap    = True,
                    filter       = ('{BasicName == "game turn"}'))
    # ----------------------------------------------------------------
    # - Historical setups
    main.addMassKey(name         = 'German historical setup',
                    icon         = 'german-icon.png',
                    buttonHotkey = key('G',CTRL_SHIFT),
                    hotkey       = key(NONE,0)+f',startGerman',
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    canDisable   = True,
                    propertyGate = globalInitDE,
                    tooltip      = 'Load (semi-) historical German setup',
                    # filter       = '{Faction=="German"}',
                    reportFormat = 'Moving German units to start-up')
    main.addMassKey(name         = 'Allied historical setup',
                    icon         = 'allied-icon.png',
                    buttonHotkey = key('A',CTRL_SHIFT),
                    hotkey       = key(NONE,0)+f',startAllied',
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    canDisable   = True,
                    propertyGate = globalInitAL,
                    tooltip      = 'Load (semi-) historical Allied setup',
                    # filter       = '{Faction=="Allied"}',
                    reportFormat = 'Moving Allied units to start-up')
    # ----------------------------------------------------------------
    # - Check for combat phase 
    main.addMassKey(name         = 'Check for declare phase',
                    buttonHotkey = checkDeclare,
                    buttonText   = '',
                    hotkey       = checkDeclare,
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}',
                    reportFormat = (f'{{{debug}?("~ Check for declare"):""}}'))
    # ----------------------------------------------------------------
    # - Check for combat phase 
    main.addMassKey(name         = 'Check for combat phase',
                    buttonHotkey = checkResolve,
                    buttonText   = '',
                    hotkey       = checkResolve,
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}',
                    reportFormat = (f'{{{debug}?("~ Check for resolve"):""}}'))
    # ----------------------------------------------------------------
    # - Check for combat phase 
    main.addMassKey(name         = 'Check for initial setup',
                    buttonHotkey = checkInitDE,
                    buttonText   = '',
                    hotkey       = checkInitDE,
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}',
                    reportFormat = (f'{{{debug}?("~ Check for initial"):""}}'))
    # ----------------------------------------------------------------
    # - Check for combat phase 
    main.addMassKey(name         = 'Check for initial invasion',
                    buttonHotkey = checkInitAL,
                    buttonText   = '',
                    hotkey       = checkInitAL,
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}',
                    reportFormat = (f'{{{debug}?("~ Check for initial"):""}}'))
    # ----------------------------------------------------------------
    # - Store defender hex 
    main.addMassKey(name         = 'Store defender hex',
                    buttonHotkey = updateHex,
                    buttonText   = '',
                    singleMap    = True,
                    hotkey       = updateHex,
                    target       = '',
                    filter       = curDef,
                    reportFormat = (f'{{{debug}?("Mass update defender hex -"'
                                    f'+{defenderHex}+"-"):""}}'))
    # ----------------------------------------------------------------
    # - Store HQ hex 
    main.addMassKey(name         = 'Store HQ hex',
                    buttonHotkey = updateHQ,
                    buttonText   = '',
                    singleMap    = True,
                    hotkey       = setHQHex,
                    target       = '',
                    filter       = f'{{Echleon==army}}', # was curBtl,
                    reportFormat = (f'{{{debug}?"Update HQ hex":""}}'))
    # ----------------------------------------------------------------
    # - Check if attack over coast 
    main.addMassKey(name         = 'Check for invasion attack',
                    buttonHotkey = updateCoastal,
                    buttonText   = '',
                    hotkey       = updateCoastal,
                    singleMap    = True,
                    target       = '',
                    filter       = curAtt,
                    reportFormat = (f'{{{debug}?("Mass update coastal "'
                                    f'+{globalCoastal}):""}}'))
    # ----------------------------------------------------------------
    # - Mark no terrain 
    main.addMassKey(name         = 'Mark for NT result',
                    buttonHotkey = noTerrainKey,
                    buttonText   = '',
                    hotkey       = noTerrainKey,
                    singleMap    = True,
                    target       = '',
                    filter       = curDef,
                    reportFormat = (f'{{{debug}?("Set no terrain on "+'
                                    f'"battle # "+{currentBattle}+" "+'
                                    f'{battleResult}):""}}'))
    # ----------------------------------------------------------------
    # - Mark no terrain 
    main.addMassKey(name         = 'Clear for NT result',
                    buttonHotkey = noTerrainClear,
                    buttonText   = '',
                    hotkey       = noTerrainClear,
                    singleMap    = True,
                    target       = '',
                    filter       = '{{Faction=="German"}}',
                    reportFormat = (f'{{{debug}?("Clear all no '
                                    f'terrain flags"):""}}'))
    # ----------------------------------------------------------------
    main.addMassKey(name         = 'Give MP',
                    buttonHotkey = giveMP,
                    hotkey       = incrMPKey,
                    buttonText   = '',
                    icon         = '',
                    reportSingle = True,
                    singleMap    = False,
                    reportFormat = (f'{{{verbose}?('
                                    f'"! German MP incremented because of '
                                    f'Allied strategic bombing"):""}}'),
                    target       = '',
                    filter       = (f'{{BasicName == "de moves"}}'),
                    tooltip      = 'Increment MP counter due to SB')
    # ----------------------------------------------------------------
    main.addMassKey(name         = 'Allied Turn MP',
                    buttonHotkey = turnMPKey+'Allied',
                    hotkey       = turnMPKey,
                    buttonText   = '',
                    icon         = '',
                    reportSingle = True,
                    reportFormat = '',
                    target       = '',
                    singleMap    = False,
                    filter       = (f'{{BasicName == "al moves"&&'
                                    f'Phase.contains("Allied")}}'),
                    tooltip      = 'Increment MP counter')
    main.addMassKey(name         = 'German Turn MP',
                    buttonHotkey = turnMPKey+'German',
                    hotkey       = turnMPKey,
                    buttonText   = '',
                    icon         = '',
                    reportSingle = True,
                    reportFormat = '',
                    target       = '',
                    singleMap    = False,
                    filter       = (f'{{BasicName == "de moves"&&'
                                    f'Phase.contains("German")}}'),
                    tooltip      = 'Increment MP counter')
    main.addMassKey(name         = 'Alternative Turn MP',
                    buttonHotkey = turnMPKey+'Alt',
                    hotkey       = updateMPKey,
                    buttonText   = '',
                    icon         = '',
                    reportSingle = True,
                    target       = '',
                    singleMap    = True,
                    filter       = f'{{WithMP==true}}',
                    reportFormat = f'{{{debug}?("Update MP from WithMP"):""}}',
                    tooltip      = 'Increment MP counter')
    # ----------------------------------------------------------------
    # - Roll for weather 
    main.addMassKey(name         = 'Roll for weather',
                    buttonHotkey = rollWeather,
                    buttonText   = '',
                    hotkey       = rollWeather,
                    singleMap    = True,
                    target       = '',
                    filter       = '{BasicName=="weather"}')
    # ----------------------------------------------------------------
    # - Clean counters based on optional flags 
    main.addMassKey(name         = 'Clean optional mulberry',
                    buttonHotkey = cleanOptional,
                    buttonText   = '',
                    hotkey       = deleteKey,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{{optMulberry}==false&&'
                                    f'Type=="base naval"}}'),
                    reportFormat = (f'{{{debug}?("~ Clear mulberry "+'
                                    f'({optMulberry}?"no":"yes")):""}}'))
    main.addMassKey(name         = 'Clean optional initiative',
                    buttonHotkey = cleanOptional,
                    buttonText   = '',
                    hotkey       = deleteKey,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{{optInitiative}==false&&'
                                    f'BasicName=="initiative"}}'),
                    reportFormat = (f'{{{debug}?("~ Clear initiative "+'
                                    f'({optInitiative}?"no":"yes")):""}}'))
    main.addMassKey(name         = 'Move in optional initiative',
                    buttonHotkey = startInitiative,
                    buttonText   = '',
                    hotkey       = startInitiative,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{{optInitiative}==true&&'
                                    f'BasicName=="initiative"}}'),
                    reportFormat = (f'{{{debug}?("~ Move initiative "+'
                                    f'({optInitiative}?"yes":"no")):""}}'))
    main.addMassKey(name         = 'Clean optional strategic',
                    buttonHotkey = cleanOptional,
                    buttonText   = '',
                    hotkey       = deleteKey,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{{optBomber}==false&&'
                                    f'Type=="fixed wing B H"}}'),
                    reportFormat = (f'{{{debug}?("~ Clear bomber "+'
                                    f'({optBomber}?"no":"yes")):""}}'))
    
    # ================================================================
    #
    # Prototypes
    #
    # ----------------------------------------------------------------
    # Clean up prototypes
    prototypes         = prototypeContainer.getPrototypes(asdict=False)
    seen       = list()
    for p in prototypes:
        if p['name'] == ' prototype':
            prototypes.remove(p)
        if p['name'] in seen:
            # print(f'Removing prototype {p["name"]}')
            prototypes.remove(p)
        seen.append(p['name'])
            

    prototypes     = prototypeContainer.getPrototypes(asdict=True)
    # ----------------------------------------------------------------
    # Modify Markers prototype
    markersP     = prototypes['Markers prototype']
    traits       = markersP.getTraits()
    mdel         = Trait.findTrait(traits,DeleteTrait.ID)
    mdel['key']  = deleteKey
    mdel['name'] = ''        
    markersP.setTraits(*traits)
    
    # ----------------------------------------------------------------
    # Modify odds prototype
    oddsP          = prototypes['OddsMarkers prototype']
    traits         = oddsP.getTraits()
    basic          = traits.pop()
    die            = Trait.findTrait(traits,CalculatedTrait.ID,
                                     key='name',value='Die')
    roll           = Trait.findTrait(traits,GlobalHotkeyTrait.ID,
                                     key='globalHotkey',
                                     value=key('6',ALT))
    res           = Trait.findTrait(traits,CalculatedTrait.ID,
                                     key='name',value='BattleResult')
    trg           = Trait.findTrait(traits,TriggerTrait.ID,
                                     key='key',value=resolveKey)
    reg           = (f'(Die+{battleFrac})>=7?"D2":'
                     f'(Die+{battleFrac})>=4?"D1":'
                     f'(Die+{battleFrac})>=1?"DR":'
                     f'(Die+{battleFrac})>=-3?"-":'
                     f'(Die+{battleFrac})>=-6?"A1":"A2"')
    bmb           = ((f'(Die1==10)?"D1+NT":'
                      f'(Die1==9)?"D1":'
                      f'(Die1>=7)?"NT":"-"') if not is44 else
                     (
                         f'(Die1>=9)?"D1+NT":'
                         f'(Die1>=7)?"D1":'
                         f'(Die1>=5)?"NT":"-"'
                     ))
    rep            = None

    for t in traits:
        if t.ID != TriggerTrait.ID: continue
        if 'NT' in t['command'] or 'NT' in t['property']:
            keys =  t.getActionKeys()
            keys.insert(-1,noTerrainKey)#Second to last, last will replace
            t.setActionKeys(keys)
        
    for t in traits:
        if t.ID != ReportTrait.ID: continue
        if not t['report'].startswith('{"` Battle # '): continue
        rep = t
        break

    if rep:
        rept = rep['report'][1:-1]
        bomr = (f'"` Bombing # "+{battleNo}+": with die roll "+Die1+'
                f'": "+{battleResult}')
        rep['report'] = (f'{{{globalBomber}?({bomr}):({rept})}}')
        
    roll['globalHotkey'] = dicesKey
    getRoll              = f'GetString("{dicesName}_result")'
    getAlRoll            = 'AlliedDice_result'
    getDeRoll            = 'GermanDice_result'
    # f'{getRoll}.replaceAll(",[0-9]+","")'
    # f'{getRoll}.replaceAll("[0-9]+,","")'
    getRoll1             = f'(Phase.contains("Allied")?{getAlRoll}:{getDeRoll})'
    getRoll2             = f'(Phase.contains("Allied")?{getDeRoll}:{getAlRoll})'
    parseInt             = 'Integer.parseInt'
    die['expression'] = (f'{{{parseInt}({getRoll1})-{parseInt}({getRoll2})}}')
    die['expression'] = (f'{{{getRoll1}-{getRoll2}}}')
    res['expression'] = (f'{{{globalBomber}==true?({bmb}):({reg})}}')

    traits.extend([
        CalculatedTrait(name       = 'Die1',
                        # expression = f'{{{parseInt}({getAlRoll})}}',
                        expression  = f'{{{getAlRoll}}}',
                        description = 'Take first die'),
        GlobalHotkeyTrait(name         = '',
                          key          = noTerrainKey,
                          globalHotkey = noTerrainKey,
                          description  = 'Mark German units for NT'),
        ReportTrait(noTerrainKey,
                    report=(f'{{{verbose}?'
                            f'("! Set no terrain result on battle # "+'
                            f'{battleNo}):""}}')),
        RestrictCommandsTrait(
            name          = 'Restrict Ctrl-Y to combat',
            hideOrDisable = RestrictCommandsTrait.DISABLE,
            expression    = (f'{{{globalResolve}==true}}'),
            keys          = [resolveKey]),
        TriggerTrait(name = '', #'Give 1MP to German on bombing',
                     command    = '',
                     key        = giveMP,
                     actionKeys = [giveMP+'Trampoline'],
                     property   = (f'{{{globalBomber}==true&&'
                                   f'{optMovePoints}==true}}')),
        GlobalHotkeyTrait(name         = '', #'Give 1 MP to German for SB',
                          key          = giveMP+'Trampoline',
                          globalHotkey = giveMP,
                          description  = 'Give German 1 MP for SB'),
        ReportTrait(giveMP,
                    report=(f'{{{debug}?("! 1MP to German due to SB"):""}}')),
        MarkTrait(name = pieceLayer, value = oddLayer),
        basic])
    tak = trg.getActionKeys()
    tak.insert(-1,giveMP)
    trg.setActionKeys(tak)
    oddsP.setTraits(*traits)
                                     
    # ----------------------------------------------------------------
    # Modify odds prototype
    btlP   = prototypes['BattleMarkers prototype']
    traits = btlP.getTraits()
    basic  = traits.pop()
    traits.append(MarkTrait(name = pieceLayer, value = btlLayer))
    btlP.setTraits(*traits,basic)
    
    # ----------------------------------------------------------------
    # Modify odds prototype
    resP   = prototypes['ResultMarkers prototype']
    traits = resP.getTraits()
    basic  = traits.pop()
    traits.append(MarkTrait(name = pieceLayer, value = resLayer))
    resP.setTraits(*traits,basic)
    
    # ----------------------------------------------------------------
    # Modify optionals prototype
    optionalP = prototypes['OptionalMarkers prototype']
    traits    = optionalP.getTraits()
    basic     = traits.pop()
    optionalP.setTraits(
        TriggerTrait(name       = '',
                     key        = toggleKey,
                     actionKeys = [setOptional],
                     property   = f'{{{globalInitDE}==false}}'),
        ClickTrait(key = toggleKey,
                   context = False,
                   whole = True,
                   description = 'Toggle optional rule'),
        ReportTrait(setOptional,
                    report=(f'{{{debug}?("! "+BasicName+" toggle "+'
                            f'Name+" "+'
                            f'(Step_Level==1?"On":"Off")):""}}')),
        # ReportTrait(setOptional,
        #             report=(f'{{{verbose}?("~ "+BasicName+"<br>"+'
        #                     f'"Initiative:   "+optInitiative+"<br>"+'
        #                     f'"FightOn:      "+optFightOn+"<br>"+'
        #                     f'"MovePoints:   "+optMovePoints+"<br>"+'
        #                     f'"Replacements: "+optReplacements+"<br>"+'
        #                     f'"Weather:      "+optWeather+"<br>"+'
        #                     f'"Reaction:     "+optReaction+"<br>"+'
        #                     f'"Bomber:       "+optBomber+"<br>"+'
        #                     f'"CAS:          "+optCAS+"<br>"+'
        #                     f'"Airborne:     "+optAirborne+"<br>"+'
        #                     f'"AA:           "+optAA+"<br>"+'
        #                     f'"National:     "+optNational+"<br>"+'
        #                     f'"Pursue:       "+optPursue+"<br>"+'
        #                     f'"ExtraEffort:  "+optExtraEffort+"<br>"+'
        #                     f'"Mulberry:     "+optMulberry+"<br>"+'
        #                     f'"Isolated:     "+optIsolated+"<br>"+'
        #                     f'"Garrison:     "+optGarrison+"<br>"):""}}')),
        basic)

    # ----------------------------------------------------------------
    # Modify controls prototype
    controlP  = prototypes['ControlMarkers prototype']
    traits    = controlP.getTraits()
    basic     = traits.pop()
    controlP.setTraits(
        PrototypeTrait('WithMP prototype'),
        #RestrictAccessTrait(sides=[]),
        # DynamicPropertyTrait(
        #     ['',setGerman,DynamicPropertyTrait.DIRECT,'{2}'],
        #     name = 'Control',
        #     numeric = True),
        # DynamicPropertyTrait(
        #     ['',setAllied,DynamicPropertyTrait.DIRECT,'{1}'],
        #     name = 'Control',
        #     numeric = True),
        # DynamicPropertyTrait(
        #     ['',setNone,DynamicPropertyTrait.DIRECT,'{3}'],
        #     name = 'Control',
        #     numeric = True),
        CalculatedTrait(
            name='Control',
            expression = '{Step_Level}'),
        CalculatedTrait(
            name='MP',
            expression = '{(Benefactor&Control)>0?RMP:0}'),
        CalculatedTrait(
            name='Faction',
            expression = '{(Control==2)?"German":"Allied"}'),
        NoStackTrait(move=NoStackTrait.NEVER_MOVE,
                     bandSelect=NoStackTrait.NEVER_BAND_SELECT),
        MarkTrait(name = pieceLayer, value = objLayer),
        # ReportTrait(
        #     updateMPKey,
        #     report=(f'{{BasicName+'
        #             f'" Update MP with"+'
        #             f'" Benefactor="+Benefactor+'
        #             f'" Control="+Control+'
        #             f'" RMP="+RMP+" "+'
        #             f'" Phase="+Phase+'
        #             f'" Faction="+Faction+" "+'
        #             f'" Benefactor&Control="+(Benefactor&Control)+" "+'
        #             f'(Phase.contains(Faction)?MP:0)}}')),
        basic)

    # ----------------------------------------------------------------
    # Create die roller prototype
    traits = [
        CalculatedTrait(
            name = 'Die',
            expression = f'{{GetProperty("{diceName}_result")}}',
            description = 'Die roll'),
        GlobalHotkeyTrait(
            name         = '',
            key          = dieRoll,
            globalHotkey = diceKey,
            description  = 'Roll dice'),
        ReportTrait(diceKey,
                    report=f'{{{verbose}?("Rolled dice {diceName}"):""}}'),
        BasicTrait()
    ]
    prototypeContainer.addPrototype(name        = f'Dice prototype',
                                    description = f'Dice prototype',
                                    traits      = traits)

    # ----------------------------------------------------------------
    # Create weather prototype
    traits = [
        PrototypeTrait(name='Dice prototype'),
        CalculatedTrait(
            name = 'Weather',
            expression = (f'{{Turn<4?(Die==10?2:1):'
                          f'Turn==5?(Die>7?2:1):'
                          f'Turn==6?(Die>3?2:1):'
                          f'Turn<10?(Die>1?2:1):'
                          f'Turn==10?(Die>3?2:1):'
                          f'Turn==11?(Die>7?2:1):'
                          f'Turn==12?(Die>8?2:1):'
                          f'1}}'),
            description = 'Resolve weather'),
        CalculatedTrait(
            name        = 'WeatherName',
            expression  = (f'{{Weather==2?"Bad weather":"Clear"}}'),
            description = 'Resolve weather'),
        GlobalPropertyTrait(
            ['',setWeather,GlobalPropertyTrait.DIRECT,'{Weather==2}'],
            name        = globalWeather, # True on bad weather
            numeric     = True,
            description = 'Set global bad weather flag'),
        TriggerTrait(name       = 'Resolve weather',
                     command    = '',
                     key        = rollWeather,
                     property   = f'{{{optWeather}==true}}',
                     actionKeys = [dieRoll,
                                   setWeather]),
        ReportTrait(setWeather,
                    report=(f'{{{debug}?("Roll weather dice "+Die+" on turn "+'
                            f'Turn+" -> "+Weather+'
                            f'" "+WeatherName+" => Bad="+'
                            f'{globalWeather}):""}}')),
        BasicTrait()
    ]
    prototypeContainer.addPrototype(name        = f'Weather prototype',
                                    description = f'Weather prototype',
                                    traits      = traits)
    
    # ----------------------------------------------------------------
    # Create terrain prototype
    board = main['mapName']
    traits = [        
        CalculatedTrait(
            name        = 'AttackPort',
            expression  = f'{{{globalPort}.contains(":"+{defenderHex}+":")}}',
	    description = 'Check if defender is in Port'),
        CalculatedTrait(
            name        = 'AttackSwamp',
            expression  = f'{{{globalSwamp}.contains(":"+{defenderHex}+":")}}',
	    description = 'Check if defender is in Swamp'),
        CalculatedTrait(
            name        = 'AttackWoods',
            expression  = f'{{{globalWoods}.contains(":"+{defenderHex}+":")}}',
	    description = 'Check if defender is in Woods'),
        CalculatedTrait(
            name        = 'AttackFortified',
            expression  = (f'{{{globalFortified}.contains(":"+'
                           f'{defenderHex}+":")}}'),
	    description = 'Check if defender is in Fortified'),
        CalculatedTrait(
            name        = 'AttackCity',
            expression  = f'{{{globalCity}.contains(":"+{defenderHex}+":")}}',
	    description = 'Check if defender is in City'),
        CalculatedTrait(
            name        = 'AttackMountains',
            expression  = (f'{{{globalMountains}.contains(":"+'
                           f'{defenderHex}+":")}}'),
	    description = 'Check if defender is in Mountains'),
        CalculatedTrait(
            name        = 'AttackFort',
            expression  = f'{{{globalFort}.contains(":"+{defenderHex}+":")}}',
	    description = 'Check if defender is in Fort'),
        CalculatedTrait(
            name        = 'InBritain',
            expression  = f'{{{globalBritain}.contains(":"+LocationName+":")}}',
	    description = 'Check if defender is in Britain'),
        CalculatedTrait(
            name        = 'OnContinent',
            expression  = (f'{{{globalContinent}.contains(":"+'
                           f'LocationName+":")}}'),
	    description = 'Check if defender is in Continent'),
        CalculatedTrait(
            name        = 'Invasion',
            expression  = (f'{{{globalInvasions}.contains(":"+'
                           f'EffectiveHex+{defenderHex}+":")}}'),
	    description = 'Check if defender is in Invasions'),
        CalculatedTrait(
            name        = 'OverRiver',
            expression  = (f'{{{globalRivers}.contains(":"+'
                           f'EffectiveHex+{defenderHex}+":")}}'),
	    description = 'Check if defender is in Rivers'),
        BasicTrait()
        ]
    prototypeContainer.addPrototype(name        = f'Terrain prototype',
                                    description = f'Terrain prototype',
                                    traits      = traits)

    prototypeContainer.addPrototype(
        name   = 'WithMP prototype',
        traits = [
            MarkTrait(name='WithMP',value='true'),
            GlobalPropertyTrait(
                ['',updateMPKey,GlobalPropertyTrait.DIRECT,
                 f'{{{globalMP}+(Phase.contains(Faction)?MP:0)}}'],
                name = globalMP,
                numeric = True),
            ReportTrait(
                updateMPKey,
                report=(f'{{{debug}?('
                        f'(Phase.contains(Faction)&&MP>0)?'
                        f'("! "+BasicName+" Update MP with "+MP+'
                        f'" -> "+{globalMP}):""):""}}')),
            BasicTrait()])
                                        
    # ----------------------------------------------------------------
    # Modify types prototype.  Set AF and DF bonuses 
    coastal = 2 if is44 else 1
    ground = ['infantry',
              'infantry motorised',
              'infantry airborne',
              'infantry mountain',
              'armoured',
              'headquarters']
    for gn in ground:
        proto  = prototypes[f'{gn} prototype']
        traits = proto.getTraits()
        basic  = traits.pop()
        mount  = ('+(AttackMountains?2:0)'
                  if 'infantry' in gn and 'motorised' not in gn else '')
        woods  = ('+(AttackWoods?-1:0)'
                  if gn == 'armoured' or gn == 'infantry motorised' else '')
        swamp  = ('+(AttackSwamp?-1:0)'
                  if gn == 'armoured' or gn == 'infantry motorised' else '')

        traits.extend([
            PrototypeTrait(name='Terrain prototype'),
            CalculatedTrait(
                name       = 'BonusDF',
                expression = (f'{{(AttackWoods?1:0)+'
                              f'(AttackCity?1:0)+'
                              f'(AttackFort?(Faction=="German"?4:1):0)+'
                              f'(AttackFortified&&Faction=="German"?2:0)+'
                              f'({globalCoastal}?{coastal}:0)'
                              f'{mount}}}'),
                description = 'DF modifiers')])

        if gn != 'headquarters':
            traits.append(
                CalculatedTrait(
                    name         = 'BonusAF',
                    expression   = (f'{{(AttackMountains?-1:0)+'
                                    f'(AttackCity?-1:0)+'
                                    f'(AttackFort?-1:0)+'
                                    f'(OverRiver?-1:0)'
                                    f'{woods}{swamp}}}'),
                    description  = 'AF modifiers'))
        traits.append(basic)
        proto.setTraits(*traits)

    suphq = ['army group', 'theatre']
    for hq in suphq:
        proto  = prototypes[f'{hq} prototype']
        traits = proto.getTraits()
        basic  = traits.pop()
        traits.extend([
            CalculatedTrait(
                name='MP',
                expression='{OnContinent==true?1:0}'),
            PrototypeTrait('WithMP prototype'),
            basic
        ])
        proto.setTraits(*traits)

    air = ['fixed wing F', r'fixed wing B M']
    for a in air:
        proto  = prototypes[f'{a} prototype']
        traits = proto.getTraits()
        basic  = traits.pop()
        traits.extend([
            PrototypeTrait(name='Terrain prototype'),
            CalculatedTrait(name         = 'BonusAF',
                            expression   = (f'{{(AttackMountains?-1:0)+'
                                            f'(AttackCity?-1:0)+'
                                            f'(AttackFort?-1:0)+'
                                            f'(AttackWoods?-1:0)+'
                                            f'(AttackSwamp?-1:0)}}'),
                            description  = 'AF modifiers'),
            basic])
        proto.setTraits(*traits)

    # ----------------------------------------------------------------
    # Current Battle unit prototype
    currentBattleP = prototypes[currentBattle]
    traits         = currentBattleP.getTraits()
    basic          = traits.pop()
    setBattle      = Trait.findTrait(traits,GlobalPropertyTrait.ID,
                                     key='name',
                                     value='wgCurrentBattle')
    getBattle      = Trait.findTrait(traits,DynamicPropertyTrait.ID,
                                     key='name',
                                     value='wgBattleNo') 
    setCmds     = setBattle.getCommands()
    setCmd      = setCmds[0]
    setKey      = setCmd[1]
    setCmd[1]   = setKey+'Real'
    setBattle.setCommands(setCmds)
    getCmds     = getBattle.getCommands()
    getCmd      = getCmds[0]
    getKey      = getCmd[1]
    getCmd[1]   = getKey+'Real'
    setBomber   = key(NONE,0)+',setBomber'
    getBomber   = key(NONE,0)+',getBomber'
    getBattle.setCommands(getCmds)
    
    traits.extend([
        GlobalPropertyTrait(
            ['',setBomber,GlobalPropertyTrait.DIRECT,'{Bomber}'],
            name    = globalBomber,
            numeric = True,
            description='Make battle bomber flag global'),
        DynamicPropertyTrait(
            ['',getBomber,GlobalPropertyTrait.DIRECT,f'{{{globalBomber}}}'],
            name    = 'Bomber',
            numeric = True,
            description='Make battle bomber flag global'),
        TriggerTrait(
            name       = '',
            key        = setKey,
            actionKeys = [setBomber,
                          setCmd[1]]),
        TriggerTrait(
            name       = '',
            key        = getKey,
            actionKeys = [getBomber,
                          getCmd[1]]),
        ReportTrait(setBomber,#Set to global
                    report = (f'{{{debug}?("! "+BasicName+" set bomber "+'
                              f'Bomber+" -> "+{globalBomber}):""}}')),
        ReportTrait(getBomber,#Get from global
                    report = (f'{{{debug}?("! "+BasicName+" get bomber "+'
                              f'Bomber+" <- "+{globalBomber}):""}}')),
        basic
    ])
    currentBattleP.setTraits(*traits)
    
   
    # ----------------------------------------------------------------
    # NT Result marker
    resNT  = game.getSpecificPieces('result marker NT')[0]
    traits = resNT.getTraits()
    basic  = traits.pop()
    basic['name'] = 'NT Result'
    resL   = game.\
        getPieceWindows()['Counters'].\
        getTabs()['Counters'].\
        getPanels()['ResultMarkers'].\
        getLists()['ResultMarkers counters']
    resL.addPieceSlot(entryName = 'NT Result',
                      traits    = [
                          DeleteTrait(name='',
                                      key=noTerrainClear),
                          ReportTrait(noTerrainClear,
                                      report='Removing NT marker'),
                          MarkTrait(name='Faction',value='German'),
                          basic],
                      gpid      = game.nextPieceSlotId(),
                      height    = resNT['height'],
                      width     = resNT['width'],
                      icon      = resNT['icon'])
                      
        
    # ----------------------------------------------------------------
    # Battle unit prototype update with forest, fortress, overriver rules
    battleUnitP = prototypes[battleUnit]
    traits      = battleUnitP.getTraits()
    basic       = traits.pop()
    effAF       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='EffectiveAF')
    effDF       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='EffectiveDF')
    oddsS       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='OddsShift')
    calAF       = Trait.findTrait(traits,GlobalPropertyTrait.ID,
                                  key='name', value=battleAF)
    calDF       = Trait.findTrait(traits,GlobalPropertyTrait.ID,
                                  key='name', value=battleDF)
    calAFCmds   = calAF.getCommands()
    calAFCmd    = calAFCmds[0]
    calAFkey    = calAFCmd[1]
    calAFCmd[1] = calAFkey+'Real'
    calDFCmds   = calDF.getCommands()
    calDFCmd    = calDFCmds[0]
    calDFkey    = calDFCmd[1]
    calDFCmd[1] = calDFkey+'Real'
    calAF.setCommands(calAFCmds)
    calDF.setCommands(calDFCmds)
    
    if effAF: traits.remove(effAF)
    if effDF: traits.remove(effDF)
    # if oddsS: traits.remove(oddsS)
    setInhibit   = key(NONE,0)+',setInhibit'
    resetInhibit = key(NONE,0)+',resetInhibit'
    inhibit      = 'Inhibit'

    toremove = []
    for trait in traits:
        if trait.ID != ReportTrait.ID: continue
        # print(trait['report'])
        if 'to total attack factor' in trait['report']:
            toremove.append(trait)
        if 'to total defence factor' in trait['report']:
            toremove.append(trait)
    for trait in toremove:
        # print('Remove',trait['report'])
        traits.remove(trait)

    skel     = PlaceTrait.SKEL_PATH()
    path     = skel.format('ResultMarkers','result marker NT')
    path     = skel.format('ResultMarkers','NT Result')
    printKey = key(NONE,0)+',print'
    traits.extend([
        PrototypeTrait(name='Dice prototype'),
        PlaceTrait(command       = '',#f'Add battle marker {i}',
                   key           = noTerrainKey+'Place',
                   markerSpec    = path,
                   markerText    = 'null',
                   xOffset       = -8,
                   yOffset       = -16,
                   matchRotation = False,
                   afterKey      = '', # self._getBattle,
                   gpid          = game.nextPieceSlotId(),
                   description   = f'Add NT marker',
                   placement     = PlaceTrait.ABOVE,
                   above         = False),
        DynamicPropertyTrait(['',noTerrainKey+'Real',
                              DynamicPropertyTrait.DIRECT,
                              f'{{true}}'],
                             ['',noTerrainClear,
                              DynamicPropertyTrait.DIRECT,
                              f'{{false}}'],
                             name        = 'NoTerrain',
                             numeric     = True,
                             value       = f'{{false}}',
                             description = 'German no terrain'),
        TriggerTrait(
            name       = 'No terrain',
            key        = noTerrainKey,
            actionKeys = [noTerrainKey+'Real',
                          noTerrainKey+'Place'],
            property   = (f'{{{battleResult}.contains("NT")&&'
                          f'Faction=="German"}}')),
        ReportTrait(noTerrainKey,
                    report=(f'{{{debug}?("~ "+BasicName+'
                            f'"Set no-terrain "+{battleResult}):""}}')),
        ReportTrait(noTerrainClear,
                    report=(f'{{{debug}?("~ "+BasicName+'
                            f'" Clear no-terrain "+NoTerrain):""}}')),
        DynamicPropertyTrait(['',setInhibit+'Real',
                              DynamicPropertyTrait.DIRECT,
                              f'{{Die>=7}}'],
                             ['',resetInhibit+'Real',
                              DynamicPropertyTrait.DIRECT,
                              f'{{false}}'],
                             name        = inhibit,
                             numeric     = True,
                             value       = f'{{false}}',
                             description = 'Inhibit air on bad weather'),
        ReportTrait(setInhibit+'Real',
                    report=(f'{{{verbose}?("~ "+BasicName+'
                            f'"Check available, die="+Die+" -> "+'
                            f'({inhibit}==true?"No":"Yes")):""}}')),
        CalculatedTrait(
            name        = 'EffectiveAF',
            expression  = f'{{{inhibit}==true?0:(CF+BonusAF)}}',
            description = 'Calculate effective CF'),
        CalculatedTrait(
            name        = 'EffectiveDF',
            expression  = (f'{{{inhibit}==true?0:'
                           f'(DF+'
                           f'(Faction=="German"&&NoTerrain?0:BonusDF))}}'),
            description = 'Calculate effectice CF'),
        CalculatedTrait(
            name        = 'EffectiveHex',
            expression  = (f'{{LocationName.contains("holding")?'
                           f'GetProperty(LocationName.replace(" holding","")'
                           f'.replace(" ","_")):LocationName}}'),
            description = 'Get effective hex (possibly from HQ)'),
        GlobalPropertyTrait(
            ['',updateHex,GlobalPropertyTrait.DIRECT,
             f'{{(IsAttacker?{defenderHex}:EffectiveHex)}}'],
            name        = defenderHex,
            numeric     = True,
            description = 'Update defender hex to this unit',
        ),
        GlobalPropertyTrait(
            ['',updateCoastal,GlobalPropertyTrait.DIRECT,
             f'{{(IsAttacker?(Invasion&&{globalCoastal}):{globalCoastal})}}'],
            name        = globalCoastal,
            numeric     = True,
            description = 'Update whether over all invading',
        ),
        GlobalPropertyTrait(
            ['',updateBomber,GlobalPropertyTrait.DIRECT,
             f'{{(Bomber==true)}}'],
            name        = globalBomber,
            numeric     = True,
            description = 'Strategic bombing mission',
        ),
        TriggerTrait(
            name       = '',
            key        = setInhibit,
            actionKeys = [dieRoll,
                          setInhibit+'Real'],
            property   = (f'{{{globalWeather}==true&&'
                          f'Type.contains("fixed wing")}}')),
        TriggerTrait(
            name       = '',
            key        = setInhibit,
            actionKeys = [resetInhibit],
            property   = (f'{{{globalWeather}==false||'
                          f'(!Type.contains("fixed wing"))}}')),
        TriggerTrait(
            name       = '',
            key        = calAFkey,
            actionKeys = [setInhibit,
                          updateBomber,
                          calAFCmd[1]]),
        TriggerTrait(
            name       = '',
            key        = calDFkey,
            actionKeys = [setInhibit,
                          calDFCmd[1]]),
        TriggerTrait(
            name       = 'Print',
            command    = 'Print',
            key        = printKey,
            property   = f'{{{debug}}}',
            actionKeys = []),
        ReportTrait(printKey,
                    report=(f'{{BasicName+":"'
                            f'+" Hex="+EffectiveHex'
                            f'+" AF="+EffectiveAF'
                            f'+" DF="+EffectiveDF'
                            f'+" Faction="+Faction'
                            f'+" Attacker="+IsAttacker'
                            f'+" Step="+Step_Level'
                            f'+" Isolated="+Isolated_Level'
                            f'}}')),
        # ReportTrait(calAFCmd[1],
        #             report = (f'{{{debug}?("! "+BasicName+" Update AF "+'
        #                       f'" Location: "+LocationName+'
        #                       f'" Defender: "+{defenderHex}+'
        #                       f'" Inhibit: "+{inhibit}+'
        #                       f'" Bomber: "+Bomber+'
        #                       f'" Swamp: "+AttackSwamp+'
        #                       f'" Woods: "+AttackWoods+'
        #                       f'" Mountains: "+AttackMountains+'
        #                       f'" City: "+AttackCity+'
        #                       f'" Fort: "+AttackFort+'
        #                       f'" Fortified: "+AttackFortified+'
        #                       f'" River: "+OverRiver+'
        #                       f'" Invasion: "+Invasion+'
        #                       f'" Coastal: "+{globalCoastal}+'
        #                       f'" -> BonusAF: "+BonusAF+'
        #                       f'" -> EffectiveAF: "+EffectiveAF+'
        #                       f'" => "+{battleAF}'
        #                       f'):""}}')),
        # ReportTrait(calDFCmd[1],
        #             report = (f'{{{debug}?("! "+BasicName+" Update DF "+'
        #                       f'" Location: "+LocationName+'
        #                       f'" Defender: "+{defenderHex}+'
        #                       f'" Inhibit: "+{inhibit}+'
        #                       f'" Swamp: "+AttackSwamp+'
        #                       f'" Woods: "+AttackWoods+'
        #                       f'" Mountains: "+AttackMountains+'
        #                       f'" City: "+AttackCity+'
        #                       f'" Fort: "+AttackFort+'
        #                       f'" Fortified: "+AttackFortified+'
        #                       f'" River: "+OverRiver+'
        #                       f'" Invasion: "+Invasion+'
        #                       f'" Coastal: "+{globalCoastal}+'
        #                       f'" -> BonusDF: "+BonusDF+'
        #                       f'" -> EffectiveDF: "+EffectiveDF+'
        #                       f'" => "+{battleDF}'
        #                       f'):""}}')),
        ReportTrait(calAFCmd[1],
                    report = (f'{{{debug}?("! "+BasicName+'
                              f'" Hex="+EffectiveHex+" ("+LocationName+")"+'
                              f'" AF="+CF+"+"+BonusAF+" ("'
                              f'+(AttackSwamp?"Swamp ":" ")'
                              f'+(AttackWoods?"Woods ":" ")'
                              f'+(AttackMountains?"Mountains ":" ")'
                              f'+(AttackCity?"Mountains ":" ")'
                              f'+(AttackFort?"Fort ":" ")'
                              f'+(OverRiver?"River ":" ")'
                              f'+")="+EffectiveAF+"=>"'
                              f'+{battleAF}'
                              f'):""}}')),
        ReportTrait(calDFCmd[1],
                    report = (f'{{{debug}?("! "+BasicName+'
                              f'" Hex="+EffectiveHex+" ("+LocationName+")"+'
                              f'" Defended="+{defenderHex}+'
                              f'" DF="+DF+"+"+BonusDF+" ("'
                              f'+(AttackWoods?"Woods ":" ")'
                              f'+(AttackMountains?"Mountains ":" ")'
                              f'+(AttackCity?"Mountains ":" ")'
                              f'+(AttackFort?"Fort ":" ")'
                              f'+(AttackFortified?"Fortified ":" ")'
                              f'+({globalCoastal}?"Coastal ":" ")'
                              f'+")="+EffectiveDF+"=>"'
                              f'+{battleDF}'
                              f'):""}}')),
        ReportTrait(updateCoastal,
                    report = (f'{{{debug}?("! "+BasicName+" Update invasion "+'
                              f'Invasion+" -> "+{globalCoastal}'
                              f'):""}}')),
        ReportTrait(updateHex,
                    report = (f'{{{debug}?("! "+BasicName+" in "+'
                              f'LocationName+" update hex -> "+'
                              f'{defenderHex}'
                              f'):""}}')),
        # ReportTrait(updateHQ,
        #             report = (f'{{{debug}?("! "+BasicName+" in "+'
        #                       f'LocationName+" update HQ hex -> "+'
        #                       f'GetProperty(BasicName.replaceAll(" ","_"))+'
        #                       f'" zone="+CurrentZone+" Board="+CurrentBoard'
        #                       f'):""}}')),
        basic
        ])
    rest = RestrictCommandsTrait(keys          = [declareKey],
                                 name          = 'Disable when not in combat',
                                 hideOrDisable = RestrictCommandsTrait.DISABLE,
                                 expression    = f'{{{globalDeclare}==true}}')
    battleUnitP.setTraits(rest,*traits)
    #battleUnitP.setTraits(*traits)

    # ----------------------------------------------------------------
    # Battle calculation update with forest, fortress, overriver rules
    limitAF     = key(NONE,0)+',limitAF'
    limitDF     = key(NONE,0)+',limitDF'
    battleCalcP = prototypes[battleCalc]
    traits      = battleCalcP.getTraits()
    basic       = traits.pop()
    calcOdds    = Trait.findTrait(traits,TriggerTrait.ID,
                                  key   = 'key',         
                                  value = calcOddsKey)
    calcFrac    = Trait.findTrait(traits,TriggerTrait.ID,
                                  key   = 'key',         
                                  value = calcFracKey)
    batFrac    = Trait.findTrait(traits,CalculatedTrait.ID,
                                 key   = 'name',         
                                 value = 'BattleFraction')
    oldFrac    = batFrac['expression'][1:-1]
    newFrac    = f'{{{globalBomber}?11:({oldFrac})}}'
    batFrac['expression'] = newFrac
    
    traits.extend([
        # --- Set not combat phase ----------------------------------
        GlobalPropertyTrait(
            ['',checkDeclare,GlobalPropertyTrait.DIRECT,
             f'{{Phase!="German combat"&&'
             f'Phase!="German movement"&&'
             f'Phase!="Allied combat"&&'
             f'Phase!="Allied movement"&&'
             f'Phase!="Allied strategic air missions"}}'],
            name = globalDeclare,
            numeric = True,
            description = 'Check for (any) declare phase'),
        # --- Set not combat phase ----------------------------------
        GlobalPropertyTrait(
            ['',checkResolve,GlobalPropertyTrait.DIRECT,
             f'{{!Phase.contains("combat")&&'
             f'Phase!="Allied strategic air missions"}}'],
            name        = globalResolve,
            numeric     = True,
            description = 'Check for (any) resolve phase'),
        # --- Set not initial setup ----------------------------------
        GlobalPropertyTrait(
            ['',checkInitDE,GlobalPropertyTrait.DIRECT,
             f'{{Phase!="{phaseNames[0]}"||Turn!=1}}'],
            name        = globalInitDE,
            numeric     = True,
            description = 'Check for initial phase phase'),
        # --- Set not initial setup ----------------------------------
        GlobalPropertyTrait(
            ['',checkInitAL,GlobalPropertyTrait.DIRECT,
             f'{{Phase!="{phaseNames[5]}"||Turn!=1}}'],
            name        = globalInitAL,
            numeric     = True,
            description = 'Check for first AL invasion phase'),
        # --- Reset over coast line attack ---------------------------
        GlobalPropertyTrait(
            # Assume invasion
            ['',resetCoastal,GlobalPropertyTrait.DIRECT,'{true}'],
            name        = globalCoastal,
            numeric     = True,
            description = 'Reset invasion flag to true',
        ),
        # --- Update hex of defenders --------------------------------
        GlobalHotkeyTrait(
            name         = '',
            key          = optKey,
            globalHotkey = optKey,
            description  = 'Show optional rules'),
        # --- Update hex of defenders --------------------------------
        GlobalHotkeyTrait(
            name         = '',
            key          = updateHex,
            globalHotkey = updateHex,
            description  = 'Ask GKC to update defender hex'),
        # --- Update hex of HQ (not used yet) ------------------------
        GlobalHotkeyTrait(
            name         = '',
            key          = updateHQ,
            globalHotkey = updateHQ,
            description  = 'Ask GKC to update HQ hex'),
        # --- Update invasion flag -----------------------------------
        GlobalHotkeyTrait(
            name         = '',
            key          = updateCoastal,
            globalHotkey = updateCoastal,
            description  = 'Ask GKC to update invasion'),
        # --- Limit total AF -----------------------------------------
        GlobalPropertyTrait(
            ['',limitAF,GlobalPropertyTrait.DIRECT,
             f'{{{battleAF}>10?10:({battleAF}<0?0:{battleAF})}}'],
            name        = battleAF,
            numeric     = True,
            description = 'Limit AF to from 0 to 10'),
        # --- Limit total DF -----------------------------------------
        GlobalPropertyTrait(
            ['',limitDF,GlobalPropertyTrait.DIRECT,
             f'{{{battleDF}>10?10:({battleDF}<0?0:{battleDF})}}'],
            name        = battleDF,
            numeric     = True,
            description = 'Limit DF to from 0 to 10'),
        # --- Reports ------------------------------------------------
        ReportTrait(checkInitDE,
                    report = (f'{{{debug}?("~ Check for initial setup "+Turn+'
                              f'"-"+Phase+" -> {globalInitDE}="+'
                              f'{globalInitDE}):""}}')),
        ReportTrait(checkInitAL,
                    report = (f'{{{debug}?("~ Check for initial setup "+Turn+'
                              f'"-"+Phase+" -> {globalInitAL}="+'
                              f'{globalInitAL}):""}}')),
        ReportTrait(checkDeclare,
                    report = (f'{{{debug}?("~ Check for declare phase "+'
                              f'Phase+" -> {globalDeclare}="+'
                              f'{globalDeclare}):""}}')),
        ReportTrait(checkResolve,
                    report = (f'{{{debug}?("~ Check for resolve phase "+'
                              f'Phase+" -> {globalResolve}-"+'
                              f'{globalResolve}):""}}')),
        ReportTrait(updateHex,
                    report = (f'{{{debug}?("~ "+BasicName+" Update def hex "+'
                              f'{defenderHex}):""}}')),
        ReportTrait(updateCoastal,
                    report = (f'{{{debug}?("~ "+BasicName+" Update coastal "+'
                              f'{globalCoastal}):""}}')),
        ReportTrait(limitAF,
                    report = (f'{{{debug}?("~ "+BasicName+" Limit AF "+'
                              f'{battleAF}):""}}')),
        ReportTrait(limitDF,
                    report = (f'{{{debug}?("~ "+BasicName+" Limit DF "+'
                              f'{battleDF}):""}}')),
        basic
        ])
    keys = calcFrac.getActionKeys()
    calcFrac.setActionKeys(keys[:-1]+[limitAF,limitDF]+keys[-1:])
    keys = calcOdds.getActionKeys()
    calcOdds.setActionKeys([resetCoastal,
                            updateHQ,
                            updateHex,
                            updateCoastal]+keys)
    battleCalcP.setTraits(*traits)
    
    # ----------------------------------------------------------------
    #
    # Create an extra Allied air prototype
    #
    elim = SendtoTrait(mapName     = 'Board',
                       boardName   = 'Board',
                       name        = 'Return to base',
                       restoreName = '',
                       restoreKey  = '',
                       destination = 'G', #R for region
                       position    = 'B12')
    prototypeContainer\
        .addPrototype(
            name        = bomberPrototype,
            traits      = [
                PrototypeTrait(name=battleUnit),
                DeleteTrait(key=deleteKey,name=''),
                MarkTrait(name='IsAttacker',     value='true'),
                MarkTrait(name='EffectiveAF',    value='0'),
                MarkTrait(name='EffectiveDF',    value='0'),
                MarkTrait(name='AttackPort',     value='false'),
                MarkTrait(name='AttackSwamp',    value='false'),
                MarkTrait(name='AttackWoods',    value='false'),
                MarkTrait(name='AttackFortified',value='false'),
                MarkTrait(name='AttackCity',     value='false'),
                MarkTrait(name='AttackMountains',value='false'),
                MarkTrait(name='AttackFort',     value='false'),
                MarkTrait(name='InBritain',      value='false'),
                MarkTrait(name='OnContinent',    value='false'),
                MarkTrait(name='Invasion',       value='false'),
                MarkTrait(name='OverRiver',      value='false'),
                MarkTrait(name='Bomber',         value='true'),
                elim,
                # Add a command "Bomb", which will forward to
                # GKC which delegates to all selected.
                # Selected defender (only 1) is then marked
                # for combat, as is this. 
                MarkTrait('Faction',    'Allied'),
                MarkTrait(name = pieceLayer, value = unitLayer),
                BasicTrait()],
            description = 'Prototype of Allied air units')
    
    # ----------------------------------------------------------------
    #
    mp = { 'Allied': { 'short': 'al',
                       'start': (24 if is44 else 18),
                       'turn':  (13 if is44 else 11),
                       'cnt':   globalMP },
           'German': { 'short': 'de',
                       'start': (5 if is44 else 8),
                       'turn':  (7 if is44 else 8),
                       'cnt':   globalMP },
          }
    for faction, m in mp.items():
        incr  = []
        decr  = []
        shrt  = m['short']
        strt  = m['start']
        turn  = m['turn']
        glb   = m['cnt']
        iCode = f'{{"{shrt} moves "+((Value>=27)?Value:(Value+1))}}'
        dCode = f'{{"{shrt} moves "+((Value<=0)?Value:(Value-1))}}'
        vCode = (f'{{LocationName=="{shrt} moves"?{strt}:'
                 f'Integer.parseInt(LocationName.replaceAll("[^0-9]",""))}}')
        traits = [
            CalculatedTrait(
                name        = 'Value',
                expression  = vCode,
                description ='Calculate current value'),
            SendtoTrait(
                mapName     = f'{faction} OOB',
                boardName   = f'{faction} OOB',
                name        = '',
                restoreName = '',
                restoreKey  = '',
                destination = 'R', #R for region
                region      = iCode,
                key         = incrMPKey),
            SendtoTrait(
                mapName     = f'{faction} OOB',
                boardName   = f'{faction} OOB',
                name        = '',
                restoreName = '',
                restoreKey  = '',
                destination = 'R', #R for region
                region      = dCode,
                key         = decrMPKey),
            GlobalHotkeyTrait(
                name         = '',
                key          = turnMPKey+'Trampoline',
                globalHotkey = turnMPKey+'Alt',
                description  = 'Ask GKC to MPs'),
            GlobalPropertyTrait(
                ['',resetMPKey,GlobalPropertyTrait.DIRECT,f'{{0}}'],
                name        = glb,
                numeric     = True,
                description = f'{faction} MPs'),
            TriggerTrait(
                name        = 'Fixed increment',
                command     = '',
                key         = fixedMPKey,
                actionKeys  = [incrMPKey],
                loop        = True,
                count       = turn),
            TriggerTrait(
                name        = 'Turn MP increment',
                command     = '',
                key         = turnMPKey,
                actionKeys  = [fixedMPKey],
                property    = f'{{{optMovePoints}==false}}'),
            TriggerTrait(
                name        = 'Alt increment',
                command     = '',
                key         = turnMPKey+'Global',
                actionKeys  = [incrMPKey],
                loop        = True,
                count       = f'{{{glb}}}'),
            TriggerTrait(
                name        = 'Turn MP increment',
                command     = '',
                key         = turnMPKey,
                actionKeys  = [resetMPKey,
                               turnMPKey+'Trampoline',
                               turnMPKey+'Global'],
                property    = f'{{{optMovePoints}==true}}'),
            ReportTrait(
                incrMPKey,decrMPKey,
                report = (f'{{{debug}?("~ "+BasicName+" "+Value):""}}')),
            ReportTrait(
                turnMPKey,
                report = (f'{{{debug}?("! "+BasicName+" "+Value+'
                          f'" to be incremented"):""}}')),
            ReportTrait(
                turnMPKey+'Trampoline',
                report = (f'{{{debug}?("! "+BasicName+" "+Value+'
                          f'" trampoline to GCK -> "+{glb}):""}}')),
            ReportTrait(
                turnMPKey+'Global',
                report = (f'{{"` {faction} MPs incremented automatically by "+'
                          f'{glb}}}')),
            ReportTrait(
                fixedMPKey,
                report = (f'{{"` {faction} MPs incremented automatically by '
                          f'{turn}"}}')),
            MarkTrait(name='Faction',value=faction),
            MarkTrait(name='Type',value='moves'),
            BasicTrait()]
        prototypeContainer\
            .addPrototype(name        = f'{faction} moves prototype',
                          description = f'{faction} moves prototype',
                          traits      = traits)

        oob = maps[f'{faction} OOB']
        oob.addMassKey(name='Flip',
                       buttonHotkey = toggleStep,
                       hotkey       = stepKey,
                       icon         = 'flip-icon.png',
                       tooltip      = 'Flip selected units')
        oob.addMassKey(name         = 'Decrement MP',
                       buttonHotkey = key('-',ALT),
                       hotkey       = decrMPKey,
                       buttonText   = '',
                       icon         = '/icons/32x32/go-previous.png',
                       reportSingle = True,
                       reportFormat = '',
                       target       = '',
                       filter       = f'{{BasicName == "{shrt} moves"}}',
                       tooltip      = 'Decrement MP counter')
        oob.addMassKey(name         = 'Increment MP',
                       buttonHotkey = key('=',ALT_SHIFT),
                       hotkey       = incrMPKey,
                       buttonText   = '',
                       icon         = '/icons/32x32/go-next.png',
                       reportSingle = True,
                       reportFormat = '',
                       target       = '',
                       filter       = f'{{BasicName == "{shrt} moves"}}',
                       tooltip      = 'Increment MP counter')
        # oob.addMassKey(name         = 'Turn MP',
        #                buttonHotkey = turnMPKey,
        #                hotkey       = turnMPKey,
        #                buttonText   = '',
        #                icon         = '',
        #                reportSingle = True,
        #                reportFormat = '',
        #                target       = '',
        #                singleMap    = False,
        #                filter       = (f'{{BasicName == "{shrt} moves"&&'
        #                                f'Phase.contains("{faction}")}}'),
        #                tooltip      = 'Increment MP counter')
        
            
        
    # ----------------------------------------------------------------
    #
    # Remove 'Eliminate' trait from faction traits.  We will replace
    # that trait on each unit with a trait that moves the unit back to
    # its place on the OOB.
    #
    # We create the traits once, since they will be copied to the prototypes
    # Phases to allow the isolate command
    enbIso   = {'Allied': [phaseNames[alliedSup],phaseNames[germanSup]],
                'German': [phaseNames[alliedSup],phaseNames[germanSup]] }
    aexpr    = '{Type=="headquarters"&&Echelon=="army"}'
    elimActions = [resetStep,
                   toggleStep,
                   resetIsolated,
                   eliminateCmd]

    more = [
        RestrictCommandsTrait(name          = 'Restrict Ctrl-R on optional',
                              hideOrDisable = RestrictCommandsTrait.HIDE,
                              expression    = (f'{{{optReplacements}==false}}'),
                              keys          = [restoreKey]),
        RestrictCommandsTrait(name          = 'Restrict Ctrl-R on phase',
                              hideOrDisable = RestrictCommandsTrait.DISABLE,
                              expression    = (f'{{{optReplacements}==false||'
                                               f'!Phase.contains(Faction+'
                                               f'" replacements")}}'),
                              keys          = [restoreKey]),
        RestrictCommandsTrait(name          = 'Restrict Ctrl-Shift-I',
                              hideOrDisable = RestrictCommandsTrait.DISABLE,
                              expression    = '{!Phase.contains("supply")}',
                              keys          = [insupplyKey]),
        TriggerTrait(name       = 'Real step loss',
                     command    = '',
                     key        = stepOrElim,
                     actionKeys = [toggleStep],
                     property   = ('{Step_Level==1&&'
                                   'GetString("ReducedCF")'
                                   '.length()>0}')),
        TriggerTrait(name       = 'Real eliminate',
                     command    = '',
                     key        = stepOrElim,
                     actionKeys = [eliminateKey],
                     property   = ('{Step_Level==2||'
                                   'GetString("ReducedCF")'
                                   '.length()==0}')),
        TriggerTrait(name       = 'Step loss or eliminate',
                     command    = 'Step loss',
                     key        = stepKey,
                     actionKeys = [stepOrElim]),
        TriggerTrait(name       = 'Replenish a unit',
                     command    = 'Reinforce',
                     key        = restoreKey,
                     actionKeys = [resetStep],
                     property   = (f'{{Step_Level==2}}')),
        TriggerTrait(name       = 'Eliminate unit',
                     command    = 'Eliminate',
                     key        = eliminateKey,
                     actionKeys = elimActions),
        LayerTrait(['','isolated-mark.png','twice-mark.png'],
                   ['','Isolated +','Twice isolated +'],
                   activateName = '',
                   activateMask = '',
                   activateChar = '',
                   increaseName = 'Isolated',
                   increaseMask = CTRL,
                   increaseChar = 'I',
                   decreaseName = '',
                   decreaseMask = '',
                   decreaseChar = '',
                   resetKey     = resetIsolated,
                   under        = False,
                   underXoff    = 0,
                   underYoff    = 0,
                   name         = 'Isolated',
                   loop         = False,
                   description  = '(Un)Mark unit as isolated',
                   always       = False,
                   activateKey  = '',
                   increaseKey  = isolatedKey,
                   decreaseKey  = '',
                   scale        = 1),
        TriggerTrait(name       = 'Remove isolation mark',
                     command    = 'In supply',
                     key        = insupplyKey,
                     actionKeys = [resetIsolated],
                     property   = (f'{{Isolated_Level>=2}}')),
        ReportTrait(eliminateKey,
                    report=(f'{{{debug}?("~ "+BasicName'
                            f'+" eliminated"):""}}')),
        ReportTrait(toggleStep,
                    report=(f'{{{debug}?("~ "+BasicName'
                            f'+" step loss"):""}}')),
        ReportTrait(restoreKey,
                    report=(f'{{{debug}?("~ "+BasicName'
                            f'+" restored"):""}}')),
        ReportTrait(isolatedKey,
                    report=(f'{{{debug}?("~ "+BasicName'
                            f'+" isolated "+Isolated_Level):""}}')),
        ReportTrait(isolatedKey,
                    report=(f'{{{debug}?("~ "+BasicName'
                            f'+" isolated "+Isolated_Level):""}}')),
    ]
    restrict = \
        RestrictCommandsTrait(name           ='Restrict isolated command',
                              hideOrDisable  = RestrictCommandsTrait.DISABLE,
                              expression     = '',
                              keys           = [isolatedKey])
    arestrict =  \
        RestrictCommandsTrait(name           = 'Restrict holding',
                              hideOrDisable  = 'Hide',
                              expression     = aexpr,
                              keys           = [])
        
    # ----------------------------------------------------------------
    # Create a prototype to send a unit to army HQ holding box
    detachKey = ''
    for faction, Armies in armies.items():
        factionp = prototypes[f'{faction} prototype']
        ftraits  = factionp.getTraits()
        fbasic   = ftraits.pop()

        # Remove traits we don't want nor need
        felim    = Trait.findTrait(ftraits,SendtoTrait.ID,
                                   key = 'name',
                                   value = 'Eliminate')
        fdel     = Trait.findTrait(ftraits,DeleteTrait.ID)
        fdel['key'] = deleteKey
        fdel['name'] = ''
        if felim is not None: ftraits.remove(felim)
        
        phs = enbIso.get(faction,None)
        restrict['expression'] = ''
        if phs is not None:
            restrict['expression'] = \
                '{(' + '&&'.join(f'Phase!="{p}"' for p in phs) + ')}'
        
        arestrict['keys'] = []
        acmds             = []
        atraits           = []
        asubmenu          = SubMenuTrait(subMenu = 'Attach to ...',
                                         keys    = [])
        atgt              = []
        
        for a in Armies:
            hldm = 'Board' # Was f'{faction} OOB'
            hldn = f'{a} holding'
            hldN = f'{a.upper()}'
            hkey = key(NONE,0)+f',{a} holding'
            hold = SendtoTrait(name        = hldN,
                               key         = hkey,
                               mapName     = hldm,
                               boardName   = hldm,
                               restoreName = f'Detach',
                               restoreKey  = detachKey,
                               destination = 'R',
                               region      = hldn)
            atraits.append(hold)
            acmds.append(hkey)
            atgt.append(hldN)

        asubmenu.setKeys(atgt)
        arestrict.setKeys(acmds)

        ftraits.append(restrict)
        ftraits.append(arestrict)
        ftraits.extend(more)
        ftraits.extend(atraits)
        ftraits.append(asubmenu)
        ftraits.append(MarkTrait(name = pieceLayer, value = unitLayer))
        ftraits.append(fbasic)
        
        factionp.setTraits(*ftraits)

    # ----------------------------------------------------------------
    # Get all pieces and add Eliminate action that moves the unit to
    # the OOB. This is also where we add the traits to move the game
    # turn marker on the turn track.
    #
    # Allied historical setup 
    alliedHist = {}
    pieces  = game.getPieces(asdict=False)
    germans = ['de ','ss ', 'ob ']
    allies  = ['al ','br ','ca ','fr ','us ', 'sha', 'mul']
    phases  = { f: phaseNames[0] for f in allies }
    phases.update({f: phaseNames[germanNo] for f in germans })
    hist    = {
        'German': {
            'de 1 arhq':	('J19',0),
            'de 5pz arhq':	('F12',0),
            'de 7 arhq':	('E15',0),
            'de 15 arhq':	('E11',0),
            'de 19 arhq':	('C18',0),
            'ss 1 a':		('de 5pz arhq holding',0), # ('F12',0),
            'de 47 a':		('de 15 arhq holding',0), # ('E11',0),
            'de 2 abi':		('D12',0),
            'de 4lw i':		('E14',0),
            'de 25 i':		('I20',0),
            'de 62 i':		('F18',0),
            'de 64 i':		('B19',0),
            'de 66 i':		('D14',0),
            'de 67 i':		('P17',0),
            'de 74 i':		('D8', 0),
            'de 81 i':		('E13',0),
            'de 82 i':		('E10',0),
            'de 84 i':		('D10',0),
            'de 85 i':		('de 7 arhq holding',0), # ('E15',0),
            'de 86 i':		('D15',0),
            'de 88 i':		('de 1 arhq holding',0), # ('J19',0),
            'de 89 i':		('de 19 arhq holding',0), # ('C18',0),
            'ss 2 a':       	('G7', 0),
            'de 25 i 2':	('E4', 0),
            'de 85 i 2':	('G7', 0),
            'de luft 2':	('G13',0),
            'ss 4 a':		('E14',0),
            'de 76 a':		('E11',0),
            'de 87 i':	        ('D14',0),
        },
        'Allied': {
            'br 2 arhq':	('C14',0),
            'br 2 fw':          ('br 2 arhq holding',0), #('C14',0),
            'br 1 mi':		('ca 1 arhq holding',0), #('C14',0),
            'br 8 mi':		('ca 1 arhq holding',0), #('C14',0),
            'ca 2 mi':		('ca 1 arhq holding',0), #('C14',0),
            'br 12 mi':		('br 2 arhq holding',0), #('C14',0),
            'br 30 mi':		('br 2 arhq holding',0), #('C14',0),
            'br 21 aghq':	('br 2 arhq holding',0), #('C14',0),
            'br 1 abi':         ('E16',0),
            'us 1 arhq':	('C15',0),
            'mulberry 1':	('C15',0),
            'mulberry 2':	('C15',0),
            'us 9 fw':		('us 1 arhq holding',0), #('C15',0),
            'us 5 mi':		('us 1 arhq holding',0), #('C15',0),
            'us 6 mi':		('us 3 arhq holding',0), #('C15',0),
            'us 7 mi':		('us 1 arhq holding',0), #('C15',0),
            'us 8 mi':		('us 3 arhq holding',0), #('C15',0),
            'us 12 mi':		('us 3 arhq holding',0), #('C15',0),
            'us 15 mi':		('us 3 arhq holding',0), #('C15',0),
            'us 18 mi':		('us 3 arhq holding',0), #('C15',0),
            'us 19 mi':		('us 3 arhq holding',0), #('C15',0),
            'us 20 mi':		('us 3 arhq holding',0), #('C15',0),
            'shaef':		('us 1 arhq holding',0), #('C15',0),
            'us 18 abi':        ('D16',0),
        }
    }
    ctrl = None
    for piece in pieces:
        name    = piece['entryName'].strip()
        faction = name[:3]
        # Remove all traits from move pieces
        if name in ['al moves','de moves']:
            Faction              = 'Allied' if faction in allies else 'German'
            traits               = piece.getTraits()
            basic                = traits.pop()
            traits               = [
                PrototypeTrait(name=f'{Faction} moves prototype')]
            if faction in allies:
                traits.append(
                    SendtoTrait(name        = '',
                                key         = startAllied,
                                mapName     = 'Allied OOB',
                                boardName   = 'Allied OOB',
                                destination = 'R',
                                region      = f'al moves {1 if is44 else 4}',
                                restoreName = '',
                                restoreKey  = ''))
            traits.append(basic)
            piece.setTraits(*traits)
            
            # German and Allied pieces
        elif faction in germans+allies:
            Faction              = 'Allied' if faction in allies else 'German'
            startKey             = (startAllied if Faction=='Allied' else
                                    startGerman)
            inuse                = name in oobu[Faction]
            traits               = piece.getTraits()
            basic                = traits.pop()
            step                 = Trait.findTrait(traits, LayerTrait.ID,
                                                   'name', 'Step')

            if not inuse:
                if gverbose:
                    print(f'Removing the piece {name}')
                piece.getParent(DummyElement,checkTag=False).remove(piece)
                continue
            
            if step is not None:
                traits.remove(step)
                step['resetKey']     = resetStep
                step['resetName']    = ''
                step['increaseKey']  = toggleStep
                step['increaseName'] = ''
            else:
                print(f'--- Step layer trait not found in {name}')
            
            elim   = SendtoTrait(name        = '',
                                 key         = eliminateCmd,
                                 mapName     = f'{Faction} OOB',
                                 boardName   = f'{Faction} OOB',
                                 restoreName = '', # f'Restore {name}',
                                 restoreKey  = unelimKey,
                                 destination = 'R',
                                 region      = name)
            if 'mulberry' in name:
                traits = [MarkTrait('Faction','Allied'),
                          PrototypeTrait('Terrain prototype'),
                          CalculatedTrait(
                              name='MP',
                              expression=(f'{{({globalWeather}==false&&'
                                          f'OnContinent==true)?1:0}}')),
                          PrototypeTrait('WithMP prototype'),
                          PrototypeTrait('base naval prototype'),
                          MarkTrait(name = pieceLayer, value = unitLayer),
                          DeleteTrait('',deleteKey)] # Start afresh

            if name.endswith('arhq'):
                gname = name.replace(' ','_')
                traits.extend([
                    GlobalPropertyTrait(
                        ['',setHQHex,GlobalPropertyTrait.DIRECT,
                         f'{{LocationName}}'],
                        name        = gname,
                        numeric     = False,
                        description = f'Location of {name}'),
                    ReportTrait(
                        setHQHex,
                        report=f'{{{debug}?(BasicName+" at "+{gname}):""}}')])
                    
            traits.append(elim)

            hx, _ = hist[Faction].get(name,(None, -1))
            for trait in traits:
                if trait.ID == MarkTrait.ID:
                    if hx is None and trait['name'] == 'upper right':
                        st = trait['value']
                        st = sub('[^=]+=','',st).strip()
                        hx = st
                    if trait['name'] == 'upper left':
                        st  = trait['value']
                        st2 = sub('[^=]+=','',st).strip()
                        try:
                            tn = int(st2)
                        except:
                            tn = -1
            # Some specific exceptions
            if not is44 and name in ['de 62 i','us 18 abi','us 12 mi']:
                hx = None

            if name == 'ss 2 a' and is44:
                tn = 0

            # bombers 
            if name.endswith('bw'): # Bombers
                step['increaseName'] = ''
                # Change from Allied prototype to Allied air prototype
                # We don't want move trail, etc.
                factrait = Trait.findTrait(traits,PrototypeTrait.ID,
                                           key = 'name',
                                           value = 'Allied prototype')
                if factrait is not None:
                    factrait['name'] = bomberPrototype

            if tn == 0 and hx is not None and hx != '':
                # print(f'Start hex of {name} is set to {hx}')
                dest = 'R' if 'holding' in hx else 'G'
                pos  = hx if dest == 'G' else ''
                reg  = hx if dest == 'R' else ''
                
                startup = SendtoTrait(name        = 'Start-up',
                                      key         = startKey,
                                      mapName     = 'Board',
                                      boardName   = 'Board',
                                      destination = dest,
                                      region      = reg,
                                      restoreName = '',
                                      restoreKey  = '',
                                      position    = pos)
                traits.append(startup)

            # Remove duplicate prototypes - e.g., "infantry" from
            # "infantry airborne"
            dupl = ['infantry motorised',
                    'infantry airborne',
                    'infantry air defence',
                    'infantry mountain']
            for dup in dupl:
                if Trait.findTrait(traits,PrototypeTrait.ID,
                                   key='name',
                                   value=f'{dup} prototype') is None:
                    continue
                # print(dup)
                for o in dup.split(' '):
                    t = Trait.findTrait(traits,PrototypeTrait.ID,
                                        key='name',
                                        value=f'{o} prototype')
                    if t is not None:
                        # print(f'Removing {o} from {dup} piece')
                        traits.remove(t)
                        
            # Remove extra from HQ prototypes - e.g., "armoured" from
            # "heaquarters armoured"
            for a in ['armoured', 'airborne']:
                ahq = Trait.findTrait(traits,PrototypeTrait.ID,
                                      key='name',
                                      value=f'headquarters {a} prototype')
                if ahq is not None: ahq['name'] = 'headquarters prototype'
                # if ahq is not None: traits.remove(ahq)
            

            # Get the enable isolation phase 
            ph = enbIso.get(faction,None)
            if ph is not None and 'mulberry' not in name:
                rest = RestrictCommandsTrait('Restrict step command',
                                             hideOrDisable = 'Disable',
                                             expression = f'{{Phase!="{ph}"}}',
                                             keys       = [stepKey])
                traits.insert(0,rest)

            # Add step trait back in
            if 'mulberry' not in name and step is not None: traits.append(step)

            # Set traits 
            traits.append(basic)
            piece.setTraits(*traits)

        # Add prototype traits to game turn marker.  Note, we
        # remove the basic piece trait from the end of the list,
        # and add it in after adding the other traits: The basic
        # piece trait _must_ be last.
        elif name == 'game turn':
            gtraits  = piece.getTraits()
            markers  = Trait.findTrait(gtraits,
                                       PrototypeTrait.ID,
                                       key = 'name',
                                       value = 'Markers prototype')
            flip     = Trait.findTrait(gtraits,LayerTrait.ID)
            if markers is not None:
                gtraits.remove(markers)
            flip['increaseName'] = ''
            
            gbasic   = gtraits.pop()
            # Add the prototypes we created above 
            gtraits.extend([PrototypeTrait(pnn + ' prototype')
                            for pnn in turnp])
            # Do not allow movement
            gtraits.append(RestrictAccessTrait(sides=[],
                                               description='Cannot move'))
            gtraits.append(gbasic)
            piece.setTraits(*gtraits)

        elif name == 'weather':
            # Q7
            if gverbose:
                print('Placing weather chit at initial position at Q7')
                
            traits = piece.getTraits()
            basic  = traits.pop()

            # Add weather prototype 
            traits.append(PrototypeTrait('Weather prototype'))

            # Remove markers prototype 
            prot   = Trait.findTrait(traits,PrototypeTrait.ID,
                                     key   = 'name',
                                     value = 'Markers prototype')
            if prot is not None: traits.remove(prot)

            # Modify layer prototype to follow property 
            step   = Trait.findTrait(traits,LayerTrait.ID,
                                     key   = 'name',
                                     value = 'Step')
            step['activateName'] = ''
            step['resetName']    = ''
            step['loop']         = False
            step['activateKey']  = ''
            step['increaseKey']  = ''
            step['decreaseKey']  = ''
            step['follow']       = True
            step['expression']   = '{Weather}'
            step['expression']   = f'{{{globalWeather}==true?2:1}}'
            traits.append(basic)
            piece.setTraits(*traits)

            # Place above turn track
            main.addAtStart(name = 'Weather',
                            owningBoard = 'Board',
                            location = 'T6').addPiece(piece)
                            
        elif name == 'initiative':
            # R6
            traits = piece.getTraits()
            basic  = traits.pop()
            step   = Trait.findTrait(traits, LayerTrait.ID,
                                     'name', 'Step')
            step['increaseName'] = 'Use'
            prot   = Trait.findTrait(traits,PrototypeTrait.ID,
                                     'name', 'Markers prototype')
            if prot: traits.remove(prot)

            traits.append(
                SendtoTrait(name        = '',
                            key         = startInitiative,
                            mapName     = 'Board',
                            boardName   = 'Board',
                            destination = 'G',
                            restoreName = '',
                            restoreKey  = '',
                            position    = 'R6'))
            piece.setTraits(*traits,basic)
                

            if  not is44:
                if gverbose:
                     print('Placing initiative chit in German OOB')
                maps['German OOB'].addAtStart(name = 'Initiative',
                                              owningBoard = 'German OOB',
                                              location = 'initiative flipped')\
                                  .addPiece(piece)

        elif name == hidden:
            traits = piece.getTraits()
            basic  = traits.pop()
            showN  = key(NONE,0)+',showNotes'
            initD  = key(NONE,0)+',initDice'
            traits = [
                TriggerTrait(name       = 'Show notes window for tutorial',
                             command    = '',
                             key        = tutorialKey,
                             property   = f'{{{isTutorial}==true}}',
                             actionKeys = [showN]),
                # TriggerTrait(name       = 'Initial die roll',
                #              command    = '',
                #              key        = dicesKey,
                #              actionKeys = [initD]),
                GlobalHotkeyTrait(name         = '',
                                  key          = showN,
                                  globalHotkey = key('N',ALT),
                                  description  = 'Show notes window'),
                GlobalPropertyTrait(
                    ['',toggleBias,GlobalPropertyTrait.DIRECT,
                     f'{{!{globalNormal}}}'],
                    name        = globalNormal,
                    numeric     = True,
                    wrap        = True,
                    description = 'Toggle use of normal dice'),
                GlobalPropertyTrait(
                    ['',toggleBias,GlobalPropertyTrait.DIRECT,
                     f'{{!{globalBiased}}}'],
                    name        = globalBiased,
                    numeric     = True,
                    wrap        = True,
                    description = 'Toggle use of biased dice'),
                ReportTrait(toggleBias,
                            report = (f'{{"Dice: normal="+{globalNormal}'
                                      f'+" biased="+{globalBiased}}}')),
                # GlobalPropertyTrait(
                #     ['',dicesKey,GlobalPropertyTrait.DIRECT,'{1}'],
                #     name        = 'AlliedDice_result',
                #     numeric     = True,
                #     min         = 1,
                #     max         = 10,
                #     wrap        = True,
                #     description = 'Initial set of dice'),
                # GlobalPropertyTrait(
                #     ['',dicesKey,GlobalPropertyTrait.DIRECT,'{1}'],
                #     name        = 'GermanDice_result',
                #     numeric     = True,
                #     min         = 1,
                #     max         = 10,
                #     wrap        = True,
                #     description = 'Initial set of dice'),
                # GlobalHotkeyTrait(name         = '',
                #                   key          = initD,
                #                   globalHotkey = dicesKey,
                #                   description  = 'Initial die roll'),
                # ReportTrait(showN,tutorialKey,
                #             report = f'{{"Show notes "+{isTutorial}}}'),
            ]+traits
            piece.setTraits(*traits,basic)
            
        #elif name == 'odds marker 11':
        #    traits = piece.getTraits()
        #    basic  = traits.pop()
        #    basic['filename'] = 'sb-icon.png'

        elif name.startswith('opt '):
            optName = name.replace('opt ','').strip()
            gpName  = name.replace(' ','')
            
            traits = piece.getTraits()
            basic  = traits.pop()
            step   = Trait.findTrait(traits,LayerTrait.ID,
                                     key = 'name',
                                     value = 'Step')
            step['newNames']     = ['+ enabled','+ disabled']
            step['increaseName'] = ''
            step['increaseKey']  = ''
            step['follow']       = True
            step['expression']   = f'{{{gpName}==true?1:2}}'
            traits.extend([
                MarkTrait(name='OptName',value=optName),
                GlobalPropertyTrait(
                    ["",setOptional,GlobalPropertyTrait.DIRECT,
                     f'{{!{gpName}}}'],
                    name = gpName,
                    numeric = True,
                    description = f'Toggle {gpName}'),
                basic
            ])
            piece.setTraits(*traits)
        elif name == 'control':
            ctrl = piece


    for hex,data in supplies.items():
        cp = main.addAtStart(name = f'ctrl_{hex}',
                             owningBoard = 'Board',
                             location = hex).addPiece(ctrl)
        traits = cp.getTraits()
        basic  = traits.pop()
        basic["name"] = f'control {hex}'
        step   = Trait.findTrait(traits,LayerTrait.ID,
                                 key='name',value='Step')
        lvl = 1 if hex in ['A14','B12','C12','T19'] else 2

        step['newNames'] = 'Allied +,German +'
        step.setState(level=lvl)
        traits.extend([
            MarkTrait(name='Benefactor',value=data[1]),
            MarkTrait(name='RMP',       value=data[0]),
            basic])
        cp.setTraits(*traits);
        
        
            

            
#
# EOF
#

                      
    
    
