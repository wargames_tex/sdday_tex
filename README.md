# D-Day - Smithsonian edition

[[_TOC_]]

This is my remake of the wargame _D-Day - Smithsonian edition_.  The
original game was published by Avalon Hill Game Company in 1991.

The _D-Day - Smithsonian edition_ is a variant of the classic AHGC
[D-Day](https://gitlab.com/wargames_tex/dday_tex), geared toward
new-comers to hex'n'counter board wargaming.   The game was published,
but has since gone out of print, as part of the _American History_ series
that AHGC developed together with the Smithsonian Museum in United
States of America.   Other games in the series include 

- [Battle of Bulge](https://gitlab.com/wargames_tex/sbotb_tex)
  simulating the infamous battle where the US commander stood his
  ground with the reply "Nuts" when encouraged to surrender to the
  Germans.
- [Gettysburg](https://gitlab.com/wargames_tex/sgb_tex) -
  the civil war battle 
- [Guadalcanal](https://boardgamegeek.com/boardgame/1375/guadalcanal)
  which was the start of the US pacific offensive.  This simulates
  mainly the naval part of the battle.
- [Midway](https://boardgamegeek.com/image/568159/midway) - the aerial
  battle of the islands in the middle of the Pacific Ocean 
- [Mustangs](https://boardgamegeek.com/image/6051696/mustangs) -
  dogfights during WWII. 
- [We the people](https://boardgamegeek.com/boardgame/620/we-people)
  Not really a hex'n'counter game, but a novel approach using cards
  (instead of dice) and interconnected spaces on a board.

See also Print'n'Play versions of the sister games [_Gettysburg -
Smithsonian Edition_](https://gitlab.com/wargames_tex/sgb_tex) and
[_Battle of the Bulge - Smithsonian
Edition_](https://gitlab.com/wargames_tex/sbotb_tex).

For the _other_ classic AHGC game of the same name, see
[here](https://gitlab.com/wargames_tex/dday_tex). 

See also the [$`\mathrm{\LaTeX}`$
Wargame](https://wargames_tex.gitlab.io/wargame_www/) for other
Print'n'Play as well as VASSAL modules done by myself. 

## Game mechanics 

|              |                    |
|--------------|--------------------|
| Period       | WWII               |
| Level        | Operational        |
| Hex scale    | 37km (60 miles)    |
| Unit scale   | corps (xxx)        |
| Turn scale   | 1 month            |
| Unit density | High               |
| # of turns   | 12                 |
| Complexity   | 1 of 10            |
| Solitaire    | 8 of 10            |

Features:
- Campaign
- Tactical air
- Amphibious landing
- Abstracted logistics
- Scenarios
- Optional rules

## About this rework 

This rework is entirely new.  All text and graphics is new and nothing
is copied verbatim from the original materials. 

I have restructured and rewritten the rules somewhat (the text is all
new), as well as added illustrations of the rules.

## The board 

The board is exactly too big (30.5cm x 35.5cm, or 12" x 14") to fit on
standard paper formats, so everything has been scaled down a bit
(94%). 

## The files 

The distribution consists of the following files 

- [DDaySmithsonianA4.pdf][] This is a single document that contains
  everything: The rules, charts, counters, and order of battle, and
  the board split into two parts.
  
  This document is meant to be printed on a duplex A4 printer.  The
  charts, OOB charts, counters, and board will be on separate sheets
  and can be separated from the rules.
  
- [DDaySmithsonianA4Booklet.pdf][] This document _only_ contains the
  rules.  It is meant to be printed on a duplex A4 printer with long
  edge binding.  This will produce 6 sheets which should be folded
  down the middle of the long edge and stabled to form an 24-page A5
  booklet of the rules.
  
- [boardA3.pdf][] holds the entire board on a sheet of A3 paper.
  Print this and glue on to a sturdy piece of cardboard (1.5mm poster
  carton seems a good choice). 
  
- [boardA4.pdf][] has the board split over two A4 sheets of paper.
  Cut out and glue on to sturdy cardboard.  Note that one sheet should
  be cut at the crop marks and glued over the other sheet.  Take care
  to align the sheets. 
  
- [materialsA4.pdf][] holds the charts, counters, and OOB.  It is
  meant to be printed on A4 paper.  Print and glue on to a relatively
  thick piece of cardboard (1.5mm or so) or the like.
  
  The counters are double-sided, which means one has to take care to
  align the sides.   My procedure is roughly as follows: 
  - I like to glue the full counter table on to a
	medium thick piece of cardboard (0.75mm poster carton) and let it
	dry between two heavy books. 
  - Then, once dried, I cut down the middle between the front and the
	back, _but not the whole way through_.
  - Then I fold over the back-side along the cut line and glue the
	back on to the front, and let it dry between two heavy books.  
  - Once dried I start to cut out the counters.  
    - I do that by cutting groves (i.e., not all the way through)
      along the vertical and horizontal lines.
	- Finally, I cut entirely through the cardboard along the longest
      direction, _except_ for a small bit in one end.  
    - Then, I can cut through in the other direction quite easily, and
      separate out the counters. 
  
- [hexes.pdf][] Is the whole board as a PDF.  If you have a way of
  printing such a large file, you may find this useful.

- [historical.pdf][] Rough historical setup of German and Allied forces.

If you only have access to US Letter (and possibly Tabloid) printer,
you can use the following files

| *A4 Series*                      | *Letter Series*                      |
| ---------------------------------|--------------------------------------|
| [DDaySmithsonianA4.pdf][]        | [DDaySmithsonianLetter.pdf][]  	  |
| [DDaySmithsonianA4Booklet.pdf][] | [DDaySmithsonianLetterBooklet.pdf][] |
| [materialsA4.pdf][]	           | [materialsLetter.pdf][]	          |
| [boardA4.pdf][]	               | [boardLetter.pdf][]	              |
| [boardA3.pdf][]	               | [boardTabloid.pdf][]	              |

Note, the US letter files are scaled down to 95% relative to the A4
files.  This is to fit everything in on the same scale.  This should
not be a problem for most users as most of the world is using A4
rather than the rather obscure Letter format. 

Download [artifacts.zip][] to get all files for both kinds of paper
formats. 

## VASSAL modules 

Also available are two [VASSAL](https://vassalengine.org) modules 

- [1943 Scenario][DDaySmithsonian43.vmod] for the counter-factual scenario
- [1944 Scenario][DDaySmithsonian44.vmod] for the historical scenario 

Both modules are generated from the same sources as the Print'n'Play
documents, and contains the rules as an embedded PDF.  The modules
features 

- Battle markers 
- Automatic odds markers 
- Automatic battle resolution, including for Allied strategic bombing
  missions
- Optional rules selection
- Automatic move points replenish on move points phase
- Embedded rules 
- Historical setup's (for 1944 scenario) 
- Moved and battle markers automatically cleared 
- Army holding boxes from which combat can also be automatically
  resolved. 
- Symbolic dice 
- Separate layers for units and markers 
- Tutorial (soon)

## Previews

![Board](.imgs/board.png)
![Front](.imgs/front.png)
![Charts](.imgs/charts.png)
![Counters](.imgs/counters.png)
![Allied OOB 1943](.imgs/oob-al43.png)
![Allied OOB 1944](.imgs/oob-al44.png)
![German OOB 1943](.imgs/oob-de43.png)
![German OOB 1944](.imgs/oob-de44.png)
![VASSAL module](.imgs/vassal.png)

## Articles 

- Miller,R.L., "Once more unto the Breach", 
  [_The General_, **Vol.28**, #4, p15-17](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2028%20No%204.pdf#page=15)
- Toro,P.R., "Putting up the air in D-Day", 
  [_The General_, **Vol.31**, #4,
  p57-60](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2031%20No%204.pdf#page=57)
  
## Implementation 

The whole package is implemented in $`\mathrm{\LaTeX}`$ using my
package [`wargame`](https://gitlab.com/wargames_tex/wargame_tex).
This package, combined with the power of $`\mathrm{\LaTeX}`$, produces
high-quality documents, with vector graphics to ensure that everything
scales.  Since the board is quite big, we must use
Lua$`\mathrm{\LaTeX}`$ for the formatting.

[artifacts.zip]: https://gitlab.com/wargames_tex/sdday_tex/-/jobs/artifacts/master/download?job=dist

[DDaySmithsonianA4.pdf]: https://gitlab.com/wargames_tex/sdday_tex/-/jobs/artifacts/master/file/DDaySmithsonian-A4-master/DDaySmithsonianA4.pdf?job=dist
[DDaySmithsonianA4Booklet.pdf]: https://gitlab.com/wargames_tex/sdday_tex/-/jobs/artifacts/master/file/DDaySmithsonian-A4-master/DDaySmithsonianA4Booklet.pdf?job=dist
[materialsA4.pdf]: https://gitlab.com/wargames_tex/sdday_tex/-/jobs/artifacts/master/file/DDaySmithsonian-A4-master/materialsA4.pdf?job=dist
[boardA3.pdf]: https://gitlab.com/wargames_tex/sdday_tex/-/jobs/artifacts/master/file/DDaySmithsonian-A4-master/boardA3.pdf?job=dist
[boardA4.pdf]: https://gitlab.com/wargames_tex/sdday_tex/-/jobs/artifacts/master/file/DDaySmithsonian-A4-master/boardA4.pdf?job=dist
[hexes.pdf]: https://gitlab.com/wargames_tex/sdday_tex/-/jobs/artifacts/master/file/DDaySmithsonian-A4-master/hexes.pdf?job=dist
[historical.pdf]: https://gitlab.com/wargames_tex/sdday_tex/-/jobs/artifacts/master/file/DDaySmithsonian-A4-master/historical.pdf?job=dist
[DDaySmithsonian43.vmod]: https://gitlab.com/wargames_tex/sdday_tex/-/jobs/artifacts/master/file/DDaySmithsonian-A4-master/DDaySmithsonian43.vmod?job=dist
[DDaySmithsonian44.vmod]: https://gitlab.com/wargames_tex/sdday_tex/-/jobs/artifacts/master/file/DDaySmithsonian-A4-master/DDaySmithsonian44.vmod?job=dist

[DDaySmithsonianLetter.pdf]: https://gitlab.com/wargames_tex/sdday_tex/-/jobs/artifacts/master/file/DDaySmithsonian-Letter-master/DDaySmithsonianLetter.pdf?job=dist
[DDaySmithsonianLetterBooklet.pdf]: https://gitlab.com/wargames_tex/sdday_tex/-/jobs/artifacts/master/file/DDaySmithsonian-Letter-master/DDaySmithsonianLetterBooklet.pdf?job=dist
[materialsLetter.pdf]: https://gitlab.com/wargames_tex/sdday_tex/-/jobs/artifacts/master/file/DDaySmithsonian-Letter-master/materialsLetter.pdf?job=dist
[boardTabloid.pdf]: https://gitlab.com/wargames_tex/sdday_tex/-/jobs/artifacts/master/file/DDaySmithsonian-Letter-master/boardTabloid.pdf?job=dist
[boardLetter.pdf]: https://gitlab.com/wargames_tex/sdday_tex/-/jobs/artifacts/master/file/DDaySmithsonian-Letter-master/boardLetter.pdf?job=dist


