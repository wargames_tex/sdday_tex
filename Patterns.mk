# -*- Makefile -*-
#
# Various pattern rules.  Required variables
#
# - REDIR
# - MUTE
# - LATEX
# - LATEX_FLAGS
# - PDFJAM
# - PAPER
# - SIGNATURE
# - PDFTOCAIRO
# - CAIROFLAGS
#
# --- Rules for plain PDFs -------------------------------------------
%.pdf:	%.tex
	@echo "LATEX   	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%.aux:	%.tex
	@echo "LATEX(1)	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%.pdf:	%.aux
	@echo "LATEX(2)	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

# --- Rules for A* PDFs ----------------------------------------------
%A4.aux:%.tex
	@echo "LATEX(1)	$* (A4)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%A4.pdf:%A4.aux
	@echo "LATEX(2)	$* (A4)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%A4.pdf:%.tex
	@echo "LATEX   	$* (A4)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%A3.pdf:%.tex
	@echo "LATEX   	$* (A3)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A3 $* $(REDIR)

%A3.aux:%.tex
	@echo "LATEX(1)	$* (A3)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A3 $* $(REDIR)

%A3.pdf:%A3.aux
	@echo "LATEX(2)	$* (A3)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A3 $* $(REDIR)

# --- Rules for Letter-series PDFs -----------------------------------
%Letter.pdf:	%.tex
	@echo "LATEX   	$* (Letter)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)

%Letter.aux:	%.tex
	@echo "LATEX(1)	$* (Letter)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)

%Letter.pdf:	%Letter.aux
	@echo "LATEX(2)	$* (Letter)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)

%Tabloid.pdf:	%.tex
	@echo "LATEX   	$* (Tabloid)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Tabloid $* $(REDIR)

%Tabloid.aux:	%.tex
	@echo "LATEX(1)	$* (Tabloid)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Tabloid $* $(REDIR)

%Tabloid.pdf:	%Tabloid.aux
	@echo "LATEX(2)	$* (Tabloid)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Tabloid $* $(REDIR)

# --- Rules for rules-only PDFs --------------------------------------
%Rules.aux:	%.tex
	@echo "LATEX(1)	$* (Rules)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Rules $* $(REDIR)

%Rules.pdf:	%Rules.aux
	@echo "LATEX(2)	$* (Rules)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Rules $* $(REDIR)

# --- Rules for booklets ------------------------------------------
%Booklet.pdf:%.pdf
	@echo "BOOKLET 	$*"
	$(MUTE)$(PDFJAM) 			\
		--landscape 			\
		--suffix book 			\
		--signature '$(SIGNATURE)' 	\
		$(PAPER) 			\
		--outfile $@ 			\
		-- $< '1-$(SIGNATURE)' $(REDIR)

%A3Booklet.pdf:   %A4Booklet.pdf
	@echo "BOOKLET 	$* (A3)"
	$(MUTE)$(PDFJAM) 			\
		--a3paper 			\
		--landscape 			\
		--outfile $@ 			\
		-- $< $(REDIR)

%TabloidBooklet.pdf:   %LetterBooklet.pdf
	@echo "BOOKLET 	$* (Tabloid)*"
	$(MUTE)$(PDFJAM) 			\
		--scale 1.2 			\
		--papersize '{17in,11in}' 	\
		--outfile $@ 			\
		-- $<  $(REDIR)

# --- Rules of calculating board splits ------------------------------
splitboard%.pdf:splitboard%.tex
	@echo "LATEX   	splitboard ($*)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $< $(REDIR)

splitboard%.tex:calcsplit.tex hexes.pdf $(STYLES)
	@echo "LATEX(0)	calcsplit ($*)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname calcsplit$* $< $(REDIR)


# --- Rules for images -----------------------------------------------
%.png:%-pdfjam.pdf
	@echo "PDFTOCAIRO	$* (via PDFJam)"
	$(MUTE)rm -f $@
	$(MUTE)$(PDFTOCAIRO) $(CAIROFLAGS) -singlefile -png $< $* $(REDIR)

%.png:%.pdf
	@echo "PDFTOCAIRO	$*"
	$(MUTE)$(PDFTOCAIRO) $(CAIROFLAGS) -singlefile -png $< $*

%.png:%A4.pdf
	@echo "PDFTOCAIRO	$*"
	$(MUTE)$(PDFTOCAIRO) $(CAIROFLAGS) -singlefile -png $< $*

#
# EOF
#
