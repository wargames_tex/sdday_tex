#
#
#
NAME		:= DDaySmithsonian
VERSION		:= 1.0-2
VMOD_VERSION	:= 2.2.0
DOCFILES	:= $(NAME).tex			\
		   rules.tex			\
		   materials.tex		\
		   front.tex
DATAFILES	:= board.tex			\
		   hexes.tex			\
		   oob.tex			\
		   chits.tex			\
		   tables.tex			\
		   probabilities.tex		\
		   wargame.speckle.tex

OTHER		:= 
STYFILES	:= sdd.sty			\
		   commonwg.sty
BOARDS		:= hexes.pdf	probabilities.pdf
SIGNATURE	:= 24
PAPER		:= --a4paper

TARGETSA4	:= $(NAME)A4.pdf 		\
		   $(NAME)A4Booklet.pdf 	\
		   $(NAME)A3Booklet.pdf 	\
		   materialsA4.pdf		\
		   hexes.pdf			\
		   boardA3.pdf			\
		   boardA4.pdf			\
		   historical.pdf		

TARGETSLETTER	:= $(NAME)Letter.pdf 		\
		   $(NAME)LetterBooklet.pdf 	\
		   $(NAME)TabloidBooklet.pdf 	\
		   materialsLetter.pdf		\
		   boardTabloid.pdf		\
		   boardLetter.pdf

include Variables.mk
include Patterns.mk

all:	a4 vmod
a4:	$(TARGETSA4)
letter:	$(TARGETSLETTER)
vmod:	$(NAME)44.vmod $(NAME)43.vmod 
mine:	a4 vmod cover.pdf

.imgs/counters.png:chitsA4.png
	mv $< $@
.imgs/board.png:hexes.png
	mv $< $@

.imgs/charts.png:tables.png
	mv $< $@

.imgs/front.png:frontA4.png
	mv $< $@

.imgs/oob-al43.png:oobA4.pdf
	$(PDFTOCAIRO) $(CAIROFLAGS) -png $<
	mv oobA4-1.png	.imgs/oob-al43.png
	mv oobA4-3.png	.imgs/oob-al44.png
	mv oobA4-5.png	.imgs/oob-de43.png
	mv oobA4-7.png	.imgs/oob-de44.png
	rm -f oobA4*.png

update-images:	.imgs/counters.png 	\
		.imgs/board.png 	\
		.imgs/oob-al43.png 	\
		.imgs/charts.png	\
		.imgs/front.png
#	(cd .imgs && gimp -i -b '(batch-autocrop "*.png")' -b '(gimp-quit 0)')
#
#
# Put batch-autocrop.scm in ~/.config/GIMP/<version>/scripts/
#
# BEGIN batch-autocrop.scm
# (define (batch-autocrop pattern)
#   (let* ((filelist (cadr (file-glob pattern 1))))
#     (while (not (null? filelist))
#            (let* ((filename (car filelist))
#                   (image (car (gimp-file-load RUN-NONINTERACTIVE
#                                               filename filename)))
#                   (drawable (car (gimp-image-get-active-layer image))))
#              
#              (plug-in-autocrop RUN-NONINTERACTIVE
#                                image drawable)
# END  batch-autocrop.scm
#

include Rules.mk

# These generate images that are common for all formats
wargame.speckle.pdf:	wargame.speckle.tex
hexes.pdf:		hexes.tex	wargame.speckle.pdf $(STYFILES)
cover.pdf:		cover.tex	$(BOARDS)	$(STYFILES)
historical.pdf:		historical.tex	$(DATAFILES)	$(STYFILES)
historical.pdf:		LATEX=lualatex

$(NAME)A4.pdf:		$(NAME)A4.aux
$(NAME)A4.aux:		$(DOCFILES) 	$(BOARDS)	$(STYFILES) 	
materialsA4.pdf:	materials.tex 	$(BOARDS)	$(DATAFILES) 
boardA4.pdf:		board.tex	$(BOARDS)	$(STYFILES)
boardA3.pdf:		board.tex	$(BOARDS) 	$(STYFILES)
frontA4.pdf:		front.tex       $(STYFILES)

$(NAME)Letter.pdf:	$(NAME)Letter.aux
$(NAME)Letter.aux:	$(DOCFILES) 	$(BOARDS)	$(STYFILES) 	
materialsLetter.pdf:	materials.tex 	$(BOARDS)	$(DATAFILES) 	
boardLetter.pdf:	board.tex	$(BOARDS)	$(STYFILES)
boardTabloid.pdf:	board.tex	$(BOARDS)	$(STYFILES)

$(NAME)Rules.pdf:	$(NAME)Rules.aux
$(NAME)Rules.aux:	$(DOCFILES) 	$(STYFILES) 	$(BOARDS)

# Generic dependency

$(NAME)LetterBooklet.pdf:PAPER=--letterpaper
$(NAME)LetterBooklet.pdf:SIGNATURE=24

$(NAME)44.vmod:		TUTORIAL:=Tutorial.vlog

export%.pdf:	export.tex tables.tex hexes.tex $(STYFILES)
	@echo "LATEX   	export ($*)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname export$* $< $(REDIR)

$(NAME)%.vmod:export%.pdf patch.py $(NAME)Rules.pdf $(TUTORIAL)
	@echo "VMOD    	$@"
	$(MUTE)$(EXPORT) export$*.pdf export$*.json 		\
		-p patch.py 					\
		-r $(NAME)Rules.pdf				\
		-v $(VMOD_VERSION) 				\
		-t "D-Day - Smithsonian, 19$*" 			\
		-d "19$* scenario, from LaTeX sources" 		\
		-S 1						\
		$(TUTORIAL:%.vlog=-T %.vlog)			\
		-o $@  $(EXPORT_FLAGS) $(REDIR)


CI_COMMIT_REF_NAME	:= $(VERSION)

include Docker.mk

docker-prep::
	mktextfm frunmn
	mktextfm frunbn

docker-artifacts::
	cp $(NAME)44.vmod $(NAME)-A4-$(VERSION)/
	cp $(NAME)43.vmod $(NAME)-A4-$(VERSION)/

.PRECIOUS:	export44.pdf export43.pdf DDaySmithsonianRules.pdf

#
# EOF
#

