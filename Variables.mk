# -*- Makefile *-
#
# Common variables
#
ifndef	LATEX
LATEX		:= pdflatex
endif

LATEX_FLAGS	:= -interaction=nonstopmode	\
		   -file-line-error		\
		   -synctex 15			\
		   -shell-escape
EXPORT          := $(shell kpsewhich wgexport.py)
EXPORT_FLAGS	:=
PDFTOCAIRO	:= pdftocairo
CAIROFLAGS	:= -scale-to 800
PDFJAM		:= pdfjam
REDIR		:= > /dev/null 2>&1 

SUBMAKE_FLAGS	:= --no-print-directory
ifdef VERBOSE	
MUTE		:=
REDIR		:=
SUBMAKE_FLAGS	:=
LATEX_FLAGS	:= -synctex 15	-shell-escape
EXPORT_FLAGS	:= -V
else
MUTE		:= @
REDIR		:= > /dev/null 2>&1 
endif
